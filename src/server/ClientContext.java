package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.omg.CORBA.TCKind;

import serialisers.SampleSerialiser;
import server.UnitManager.LocalUnit;
import server.UnitManager.RemoteUnit;
import util.Constants;
import util.Util;
import workerloops.AsyncExecutor;
import audio.AudioLoop;
import audio.Performable;
import messages.EndStreamW;
import messages.RegisterConnectionW;
import messages.RtAudioChunkW;
import messages.StartStreamW;
import messages.StatusMessageW;
import naga.NIOService;
import naga.NIOSocket;

public class ClientContext 
{
	CloudioConnection connection;
	RtStream stream;
	
	public ClientContext()
	{
		connection = new CloudioConnection();
	}
	
	private boolean streamActive()
	{
		if(stream == null)
			return false;
		return stream.isActive();
	}
	
	private boolean validateStartStream(StartStreamW msg)
	{
		if(streamActive() && stream.type == StartStreamW.BOUNCE_STREAM){
			connection.sendMsg(StatusMessageW.createErrorMsg("Bounce in progress on this connection"));
			return false;
		}
		
		if(UnitManager.getLocalUnit(msg.unitIds.get(0)) == null){
			connection.sendMsg(new StatusMessageW(StatusMessageW.UNIT_NOT_LOCALLY_FOUND));
			return false;
		}
		
		if(msg.unitIndex < 0 || msg.unitIndex >= msg.unitIds.size()){
			connection.sendMsg(StatusMessageW.createErrorMsg("Invalid unitIndex"));
			return false;
		}
		
		//boolean succ = unit.acquireForStream(msg.streamType);
		//if(!succ)
			//return connection.sendMsg(new StatusMessageW(StatusMessageW.DEVICE_UNIT_BUSY));	
		
		//TODO: check sessionUuid is valid, correct time and associated with msg.unitIds
		
		return true;
	}
	
	public Object startStream(final StartStreamW msg)
	{
		if(!validateStartStream(msg))
			return null;
		
		if(streamActive())
			stream.endStream();
		
		final LocalUnit unit = UnitManager.getLocalUnit(msg.unitIds.get(0));		
		
		if(msg.unitIndex == msg.unitIds.size()-1) //last or only hop in the chain
		{ 		
			//connection.sendMsg(nextStartStreamMsg(msg));				//(new StartStreamW(msg.streamType, msg.unitIds, 0, msg.sessionUuid, new StreamSettings(msg)));
			stream = RtStream.create(connection, new StreamSettings(msg), unit);	 //potentially time-consuming because the RtStream and VecSender constructors create codec a decoders and an encoder
			stream.startStream();
		} 
		else if(msg.unitIndex >= 0 && msg.unitIndex < msg.unitIds.size()-1) //if(msg.unitIds.size() > 1)
		{	
			stream = RtStream.create(new CloudioConnection(), StreamSettings.getLANstreamSettings(msg.streamType, msg.numChannels), unit); //we'll create this immediately with an unconnected CloudioConnection so that incoming chunks can be added to it as soon as possible
			
			Runnable r = new Runnable(){
				public void run() {
					InetSocketAddress nextHopAddr = UnitManager.getUnit(msg.unitIds.get(msg.unitIndex+1)).getSocketAddr(); //could be local or remote
					stream.connection.addTcpConnection(ServerUtil.connectToLocalServer(nextHopAddr, msg.unitIndex, new StreamSettings(msg), connection)); 					
					stream.startStream();  //TODO: implementation of this method is unfinished
				}
			};
			AsyncExecutor.getInstance().submit(r);
		}//else not possible?
		
		return null;
	}
	
	public void endStream(EndStreamW msg)
	{
		if(stream == null)
			return;
		stream.endStream(); //TODO: this should free up the unit. Perhaps immediately for RtStreams and waiting until end of fade-out for bounce streams
		stream = null;
	}
	
	public void handleStatusMessage(StatusMessageW msg)
	{
		
	}
	
	public void receiveChunk(RtAudioChunkW msg)
	{
		if(stream == null){
			System.out.println("Error: chunk received for inactive stream");
			return;
		}
		
		stream.receiveChunk(msg); //TODO: consider what happens when the creation of the stream is delayed by a few ms
	}
}
