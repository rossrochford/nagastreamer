package server;

import java.util.LinkedList;

import messages.RtAudioChunkW;
import messages.SerialisableMessage;
import naga.NIOSocket;

public class MultiTcpConnection implements Connection
{
	LinkedList<NIOSocket> sockets = new LinkedList<NIOSocket>();
	int socketIndex = 0;
	
	public MultiTcpConnection()
	{

	}
	
	public MultiTcpConnection(NIOSocket socket)
	{
		this.addConnection(socket);
	}
	
	public void addConnection(NIOSocket socket)
	{
		sockets.add(socket);
	}
	
	public void clearConnections()
	{
		sockets.clear();
	}
	
	public void removeConnection(NIOSocket socket)
	{
		for(int i=0; i<sockets.size(); i++)
			if(sockets.get(i).equals(socket)){
				sockets.remove(i);
				break;
			}
	}
	
//	public void sendChunk(RtAudioChunkW msg) 
//	{
//		sendMsg(msg);
//	}
	
	public void sendMsg(SerialisableMessage msg) //it should usually be an RtAudioChunkW
	{
		this.write(msg.toBytes());
		//sockets.get(socketIndex).write(msg.toBytes());		
		//socketIndex = (socketIndex + 1) % sockets.size();
	}

	public void write(byte[] bytes) 
	{
		sockets.get(socketIndex).write(bytes);		
		socketIndex = (socketIndex + 1) % sockets.size();
	}

	public boolean isConnected() 
	{
		if(sockets.size() == 0)
			return false;
		
		for(int i=0; i < sockets.size(); i++)
			if(sockets.get(i).isOpen())
				return true;
		
		return false;
	}
}