package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.LinkedList;

import util.Constants;
import workerloops.UdpReceiveLoop;
import messages.RegisterConnectionW;
import messages.RtAudioChunkW;
import messages.SerialisableMessage;
import naga.NIOService;
import naga.NIOSocket;
import naga.SocketObserver;

/*
 * Compared to CloudioConnection2, this doesn't allow UDP-only connections (e.g. a client's connection is considered UNCONNECTED until a TCP connection is added) and a server-server connection only uses tcp but we want to use the same construct for both incomming and outgoing connections
 * Also, this implementation assumes udp packets will have the 'messageNum' prefix.
 */

public class CloudioConnection 
{
	static final boolean udpNoPrefix = false; //TODO: make this switchable. For now, all udp messages have prefix.
	static final int defaultNumTcpConnections = 4;
	
	LinkedList<NIOSocket> tcpSockets = new LinkedList<NIOSocket>();
	int tcpSocketIndex = 0;
	DatagramChannel channel;
	SocketAddress dest;
	int connectionType;
	
	public CloudioConnection()
	{
		//connectionType = UNCONNECTED;
	}
	
	public CloudioConnection(NIOSocket connectedSocket)
	{
		tcpSockets.add(connectedSocket);
		//connectionType = UNCONNECTED;
	}
	
	/*public void connect(InetSocketAddress dest, String clientContextUuid, SocketObserver socketObserver)
	{
		this.connect(dest, clientContextUuid, defaultNumTcpConnections, socketObserver);
	}*/
	
	public void connect(InetSocketAddress dest, String clientContextUuid, int numConnections, SocketObserver socketObserver)
	{
		tcpSockets = new LinkedList<NIOSocket>();
		for(int i=0; i< numConnections; i++){
			NIOSocket newConn = null;
			try {
				newConn = new NIOService().openSocket(dest.getHostName(), dest.getPort());
			} catch (IOException e) {e.printStackTrace();}
			newConn.listen(socketObserver); //if we get a rejection for connection we can set this in the listener
			newConn.write(new RegisterConnectionW(clientContextUuid, Constants.serverUserUuid).toBytes());
		}
	}
	
	public void disconnect()
	{
		for(int i=0; i< tcpSockets.size(); i++)
			tcpSockets.get(i).close();
	}

	/*
	NIOSocket newConn = new NIOService().openSocket(addr.getAddress(), addr.getPort());  
	if(hopNum == 0)
		newConn.listen(new ClientFacingPacketRelayConnectionListener(controlConnection, audioConnection, incommingSS, getLocalStreamSettings(incommingSS.streamId, incommingSS.numChannels) )); //needs argument for compression type
	else
		newConn.listen(new PacketRelayConnectionListener(controlConnection, audioConnection));*/		
	/*
	public boolean isConnectedReliably()
	{
		if(dest == null)
			return false;		
		if(isDestinationIPLocal() && channel != null)
			return true;		
		for(int i=0; i < tcpSockets.size(); i++)
			if(tcpSockets.get(i).isOpen())
				return true;		
		return false;
	}
	
	public boolean isDestinationIPLocal()
	{
		return true; //use white list? or a hard-coded wildcard?
	}*/
	
	public void addTcpConnection(NIOSocket socket)
	{
		tcpSockets.add(socket);
		//updateConnectionType();
	}
	
	public void addUdpChannel(DatagramChannel channel)
	{
		this.channel = channel;
		//updateConnectionType();
	}
	
//	private void updateConnectionType()
//	{
//		if(tcpSockets.size() > 0)
//			if(channel == null)
//				connectionType = TCP_ONLY;
//			else
//				connectionType = TCP_WITH_UDP;
//		else
//			if(channel == null)
//				connectionType = UNCONNECTED;
//			//else
//				//connectionType = UDP_ONLY;
//	}
	
	public boolean isConnected()  // a connection is only considered connected if there is a connected tcp socket
	{
		for(int i=0; i < tcpSockets.size(); i++)
		{
			if(tcpSockets.get(i).isOpen())
				return true;
		}
		return false;
	}
	
	public Object sendMsg(SerialisableMessage msg) //it should usually be an RtAudioChunkW
	{
		write(msg.toBytes()); //would need to change if we decided to omit prefix from udp
		
		/*if(channel != null && msg instanceof RtAudioChunkW)
			writeUdp(msg.toBytes());
		else
			writeTcp(msg.toBytes());*/
		
		//sockets.get(socketIndex).write(msg.toBytes());//socketIndex = (socketIndex + 1) % sockets.size();
		
		return null; //for convenience so we can return in one line
	}
	
	public void write(byte[] bytes)
	{
		if(channel != null && (int)(bytes[0]) == RtAudioChunkW.messageTypeNum)
			_writeUdp(bytes);
		else
			_writeTcp(bytes);
	}
	
	private void _writeUdp(byte[] bytes)
	{
		try {
			channel.send(ByteBuffer.wrap(bytes), dest);
		} catch (IOException e) {e.printStackTrace();}
	}
	
	int tcpSendIndex = 0;
	private void _writeTcp(byte[] bytes) 
	{
		tcpSockets.get(tcpSocketIndex).write(bytes);		
		
		tcpSocketIndex = (tcpSocketIndex + 1) % tcpSockets.size();
		
		tcpSendIndex++;
		if(tcpSendIndex == 30) //every 30 messages, flush out any closed connections
		{
			for(int i=0; i < tcpSockets.size(); i++)
			{
				if(!tcpSockets.get(i).isOpen()){
					tcpSockets.remove(i);
					i--;
				}
			}
			tcpSendIndex = 0;
			//updateConnectionType();
		}
	}
}

/*

packageimport java.net.SocketAddress;
 server;

import java.util.LikedList;

import messages.RtAudioChunkW;
import messages.SerialisableMessage;
import naga.NIOSocket;

public class MultiTcpConnection implements Connection
{
	LinkedList<NIOSocket> sockets = new LinkedList<NIOSocket>();
	int socketIndex = 0;
	
	public MultiTcpConnection()
	{

	}
	
	public MultiTcpConnection(NIOSocket socket)
	{
		this.addConnection(socket);
	}
	
	public void addConnection(NIOSocket socket)
	{
		sockets.add(socket);
	}
	
	public void clearConnections()
	{
		sockets.clear();
	}
	
	public void removeConnection(NIOSocket socket)
	{
		for(int i=0; i<sockets.size(); i++)
			if(sockets.get(i).equals(socket)){
				sockets.remove(i);
				break;
			}
	}
	
//	public void sendChunk(RtAudioChunkW msg) 
//	{
//		sendMsg(msg);
//	}
	
	public void sendMsg(SerialisableMessage msg) //it should usually be an RtAudioChunkW
	{
		this.write(msg.toBytes());
		//sockets.get(socketIndex).write(msg.toBytes());		
		//socketIndex = (socketIndex + 1) % sockets.size();
	}

	public void write(byte[] bytes) 
	{
		sockets.get(socketIndex).write(bytes);		
		socketIndex = (socketIndex + 1) % sockets.size();
	}

	public boolean isConnected() 
	{
		if(sockets.size() == 0)
			return false;
		
		for(int i=0; i < sockets.size(); i++)
			if(sockets.get(i).isOpen())
				return true;
		
		return false;
	}
}*/