package server;

import serialisers.SampleSerialiser;
import messages.RtAudioChunkW;
import messages.StartStreamW;

public class StreamSettings
{
	//public int streamId;
	public int streamType; //TODO
	public int numChannels;
	public int sampleRate;
	public int chunkSize;
	public int compType;
	public int bitRate;
	
	public StreamSettings(StartStreamW msg)
	{
		this(msg.streamType, msg.numChannels, msg.sampleRate, msg.chunkSize, msg.compressionType, msg.bitRate);
	}
	
	public StreamSettings(int streamType, int numChannels, int sampleRate, int chunkSize, int compType, int bitRate)
	{
		this.streamType = streamType;
		this.numChannels = numChannels;
		this.sampleRate = sampleRate;
		this.chunkSize = chunkSize;
		this.compType = compType;
		this.bitRate = bitRate;
	}
	
	public static StreamSettings getLANstreamSettings(int streamType, int numChannels) //TODO: this should be loaded from a config file or the server
	{
		int sampleRate = 48000;
		int chunkSize = 960; //probably should be some multiple of the audio vector size
		int compType = StartStreamW.PCM_32Bit;
		int bitRate = -1;
		
		return new StreamSettings(streamType, numChannels, sampleRate, chunkSize, compType, bitRate);
	}
	
	public static boolean audioEquivalent(StreamSettings s1, StreamSettings s2)
	{
		if(s1.sampleRate == s2.sampleRate && s1.chunkSize == s2.chunkSize && s1.numChannels == s2.numChannels && s1.compType == s2.compType && s1.bitRate == s2.bitRate)
			return true;
		return false;
	}
}