package server;

import java.io.IOException;
import java.net.InetSocketAddress;

import messages.RegisterConnectionW;
import naga.NIOService;
import naga.NIOSocket;
import util.Constants;

public class ServerUtil 
{
	
	public static NIOSocket connectToLocalServer(InetSocketAddress addr, int hopNum, StreamSettings incommingSS, CloudioConnection pingBackConnection)
	{
		NIOSocket newSock = null;
		try {
			newSock = new NIOService().openSocket(addr.getAddress(), addr.getPort());
		} catch (IOException e) {e.printStackTrace();}  
		
		PacketRelayConnectionListener relayListener = (hopNum > 0) ? new PacketRelayConnectionListener(pingBackConnection) : new PacketRelayConnectionListener(pingBackConnection, incommingSS, StreamSettings.getLANstreamSettings(incommingSS.streamType, incommingSS.numChannels));
		newSock.listen(relayListener);
		
		newSock.write(new RegisterConnectionW(Constants.serverUserUuid).toBytes());
		
		return newSock;  //return new CloudioConnection(newSock);
	}

}
