package server;

import serialisers.SampleDeserialiser;
import audio.MultiVec;
import messages.RtAudioChunkW;
import messages.SerialisableMessage;
import messages.StartStreamW;
import messages.StatusMessageW;
import naga.NIOSocket;
import naga.SocketObserverAdapter;

public class PacketRelayConnectionListener extends SocketObserverAdapter //this listens for packets that come back from local servers during multi-device streaming/bouncing
{	
	CloudioConnection relayConnection;
	//StreamSettings incommingSS;
	StreamSettings outgoingSS;
	boolean requiresAudioReprocess;
	VecSender vecSender;
	SampleDeserialiser dec;
	
	public PacketRelayConnectionListener(CloudioConnection relayConnection)//also consider: owningClientContext, connectionUuid
	{
		this.relayConnection = relayConnection;
		this.requiresAudioReprocess = false;
	}
	
	public PacketRelayConnectionListener(CloudioConnection relayConnection, StreamSettings incommingSS, StreamSettings outgoingSS)
	{
		this.relayConnection = relayConnection;
		
		this.outgoingSS = outgoingSS;
		this.requiresAudioReprocess = true;
		vecSender = new VecSender(relayConnection, outgoingSS, outgoingSS.chunkSize);
		dec = SampleDeserialiser.getDecoder(incommingSS);
	}
	
//	public void connectionOpened(NIOSocket arg0) 
//	{
//		super.connectionOpened(arg0);
//	}
	
	private static boolean isTCPconnectionAuthSuccMsg(byte[] msgBytes)
	{
		int messageTypeNum = (int)msgBytes[0];
		
		if(messageTypeNum == StatusMessageW.messageTypeNum && StatusMessageW.fromBytes(msgBytes).statusNum == StatusMessageW.TCP_CONNECTION_AUTH_SUCCESS)
			return true;
		return false;
	}
	
	public void packetReceived(NIOSocket socket, byte[] packet)
	{
		if(isTCPconnectionAuthSuccMsg(packet))
			return;
		
		if(!requiresAudioReprocess){
			relayConnection.write(packet);
			return;
		}
		
		SerialisableMessage msg = SerialisableMessage.fromBytes(packet);
		
		if(msg instanceof RtAudioChunkW){
			RtAudioChunkW chunk = (RtAudioChunkW)msg;
			vecSender.sendVec(new MultiVec(dec.deserialiseSamples(chunk.audioPayload)));
		}
		else if(msg instanceof StartStreamW){
			StartStreamW s = (StartStreamW)msg;
			relayConnection.sendMsg(new StartStreamW(s.streamType,s.unitIds, s.unitIndex, s.sessionUuid, outgoingSS.sampleRate, outgoingSS.chunkSize, outgoingSS.compType, outgoingSS.bitRate));
		}
		else
			relayConnection.write(packet);
		
		//TODO: if there is an error, perhaps it should be passed to the client context that started the connection? (in which case it should be passed as a constructor argument to the listener) the problem with relaying it back is that its hard to know where the error originated. perhaps we could do both: relay back and notify local ClientContext
	}
	
	public void connectionBroken(NIOSocket nioSocket, Exception exception)
	{
		relayConnection.sendMsg(new StatusMessageW(StatusMessageW.GENERIC_ERROR_MESSAGE, "inter-server connection broken"));
	}
}


/*
newSock.listen(new ClientFacingPacketRelayConnectionListener(connection, incommingSS, StreamSettings.getLANstreamSettings(incommingSS.streamType, incommingSS.numChannels)));
else
	newSock.listen(new PacketRelayConnectionListener(connection));*/