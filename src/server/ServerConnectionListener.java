package server;
import java.util.HashMap;
import java.util.List;

import auth.UserManager;
import util.Constants;
import util.FutureCallback;
import util.Util;
import workerloops.AsyncExecutor;
import messages.EndStreamW;
import messages.RegisterConnectionW;
import messages.RtAudioChunkW;
import messages.SerialisableMessage;
import messages.StartStreamW;
import messages.StatusMessageW;
import naga.NIOSocket;
import naga.SocketObserverAdapter;


public class ServerConnectionListener extends SocketObserverAdapter //or SocketObserver?
{
	HashMap<String, ClientContext> clientContextMap;
	ClientContext clientContext = null;
	
	public ServerConnectionListener(ClientContext clientContext) //for connection initiated by server to another local server (where we already have a ClientContext that the responses should be routed to)
	{
		this.clientContext = clientContext;
	}
	
	public ServerConnectionListener(HashMap<String, ClientContext> clientContextMap) //for the general incomming connection from client to server
	{
		this.clientContextMap = clientContextMap;
	}
	
	public void packetReceived(final NIOSocket socket, byte[] packet)
	{		
		final SerialisableMessage msg = SerialisableMessage.fromBytes(packet);
		
		if (Constants.DISABLE_GC){
			Util.keepFromGC(packet);
			Util.keepFromGC(msg);
		}
		
		if(msg == null) //invalid message
			return; 
		
		if(clientContext == null){
			if(msg instanceof RegisterConnectionW == false){
				socket.write(StatusMessageW.createErrorMsg("First message must be of type RegisterConnection").toBytes());
				return;
			}	
			
			authenticateConnection2((RegisterConnectionW)msg, socket); //async task
			return;
		}
		
		//will only get here if the connection has been authenticated
		
		if(msg instanceof RtAudioChunkW)
		{
			((RtAudioChunkW)msg).timestamp();
			clientContext.receiveChunk((RtAudioChunkW)msg);
		}
		else if(msg instanceof StatusMessageW)
			clientContext.startStream((StartStreamW)msg);
		else if(msg instanceof EndStreamW)
			clientContext.endStream((EndStreamW)msg);
		else if(msg instanceof StatusMessageW)
			clientContext.handleStatusMessage((StatusMessageW)msg);
	}
	
	public void authenticateConnection2(final RegisterConnectionW regMsg, final NIOSocket socket)
	{
		Runnable r = new Runnable() {
			public void run() {			
				
				if(!UserManager.getInstance().isValidUser(regMsg.userUuid, true)){
					socket.write(StatusMessageW.createErrorMsg("Unexpected user uuid").toBytes()); //or: socket.write(new StatusMessageW(StatusMessageW.TCP_CONNECTION_AUTH_FAIL).toBytes());
					socket.close();
					return;
				}

				if(regMsg.clientContextUuid == null){
					String contextUuid = Util.uuid();
					clientContext = new ClientContext();
					clientContextMap.put(contextUuid, clientContext);						
					socket.write(null); //TODO: should return contextUuid. old: socket.write(new StatusMessageW(StatusMessageW.TCP_CONNECTION_AUTH_SUCCESS).toBytes());
				}
				else{
					clientContext = clientContextMap.get(regMsg.clientContextUuid);
					if(clientContext == null){
						socket.write(StatusMessageW.createErrorMsg("Unexpected clientContext uuid").toBytes());
						return;
					}
				}
				
				clientContext.connection.addTcpConnection(socket);
			}
		};
		AsyncExecutor.getInstance().submit(r);
	}
	
	
	//public void connectionOpened(NIOSocket arg0) 

	public void connectionBroken(NIOSocket nioSocket, Exception exception)
	{
		System.out.println("Client " + nioSocket.getIp() + " disconnected.");
	}
}
