package server;

import messages.RtAudioChunkW;
import messages.SerialisableMessage;
import messages.StatusMessageW;
import naga.NIOSocket;
import serialisers.SampleDeserialiser;
import serialisers.SampleSerialiser;

//this has been replaced with PacketRelayConnectionListener

public class ClientFacingPacketRelayConnectionListener extends PacketRelayConnectionListener 
{
	SampleSerialiser outboundSampleSerialiser; //TODO: what about sample-rate conversion?
	SampleDeserialiser inboundSampleDeserialiser;
	boolean samplesNeedConversion;
																						//if the purpose of this is to 'convert' stream settings, should we be changing stream numbers, sample rates etc. also?
	public ClientFacingPacketRelayConnectionListener(CloudioConnection connection, StreamSettings outboundStreamSettings, StreamSettings inboundStreamSettings) 
	{
		super(connection);
		
		samplesNeedConversion = inboundStreamSettings.compType != outboundStreamSettings.compType; //should also compare bit rates and sample rates etc?
		
		if(samplesNeedConversion)
		{
			System.out.println("sample format conversion not tested yet, exiting");
			System.exit(0);
			
			outboundSampleSerialiser = SampleSerialiser.getEncoder(outboundStreamSettings); //SampleSerialiser.getEncoder(compType, numChannels, chunkSize);
			inboundSampleDeserialiser = SampleDeserialiser.getDecoder(inboundStreamSettings);
		}		
	}
	
	public void packetReceived(NIOSocket socket, byte[] packet)
	{
		int messageTypeNum = (int)packet[0];
		
		if(messageTypeNum == StatusMessageW.messageTypeNum && StatusMessageW.fromBytes(packet).statusNum == StatusMessageW.TCP_CONNECTION_AUTH_SUCCESS)
			return;
		
		if(samplesNeedConversion && messageTypeNum == RtAudioChunkW.messageTypeNum)
		{
			RtAudioChunkW chunk = RtAudioChunkW.fromBytes(packet, 1);
			chunk.deserialiseSamples(inboundSampleDeserialiser);
			chunk.serialiseSamples(outboundSampleSerialiser);	//actually we probably shouldn't do this here because it'll block the 'receive thread'
			relayConnection.sendMsg(chunk);	  //problem: we may want different chunk sizes!
		}
		else
			relayConnection.write(packet);
	}
}
