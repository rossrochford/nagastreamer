package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.concurrent.TimeoutException;

import util.Pair;
import messages.RtAudioChunkW;
import messages.SerialisableMessage;

public class DatagramMsgChannel 
{
	static final int maxPacketSize = 1440; 
	static final int listenPort = 5432;
	
	static DatagramMsgChannel instance;
	
	DatagramChannel channel;
	ByteBuffer buf = ByteBuffer.allocate(maxPacketSize);
	
	public DatagramMsgChannel(int port)
	{
		try{
			channel = DatagramChannel.open();
			//channel.configureBlocking(true); //channel.socket().setSoTimeout(2000); apparently DatagramChannel doesn't actually support timeouts, you need to use the socket's read() to get this functionality
			channel.socket().bind(new InetSocketAddress(port));
		} 
		catch (IOException e){e.printStackTrace();}
	}
	
	public static DatagramMsgChannel getInstance()
	{
		if(instance == null)
			instance = new DatagramMsgChannel(listenPort);
		return instance;
	}
	
	public void sendMsg(SerialisableMessage msg, SocketAddress dest)
	{
		try {
			channel.send(ByteBuffer.wrap(msg.toBytes()), dest);
		} catch (IOException e) {e.printStackTrace();}
	}
	
	//old
/*	public void sendChunk(RtAudioChunkW msg, SocketAddress dest)
	{
			channel.send(ByteBuffer.wrap(msg.toBytesNoPrefix()), dest); //I presume we can send and receive simultaneously in different threads?		
	}*/
	
	public Pair<SocketAddress, SerialisableMessage> receive()
	{		
		buf.clear();
		
		SocketAddress source;		
		try {
			source = channel.receive(buf);
		}
		catch(IOException e) {return null;} 
		
		SerialisableMessage msg = SerialisableMessage.fromBytes(buf.array());
		
		//RtAudioChunkW chunk = RtAudioChunkW.fromBytes(buf.array(), 0);
		//return new Pair<SocketAddress, SerialisableMessage>(source, chunk);
		
		return new Pair<SocketAddress, SerialisableMessage>(source, msg);
	}
	
	/*
	public Pair<SocketAddress, RtAudioChunkW> receive()
	{		
		buf.clear();
		
		SocketAddress source;		
		try {
			source = channel.receive(buf);
		}
		catch(IOException e) {return null;} 
		
		RtAudioChunkW chunk = RtAudioChunkW.fromBytes(buf.array(), 0);
		
		return new Pair<SocketAddress, RtAudioChunkW>(source, chunk);		
	}*/
}