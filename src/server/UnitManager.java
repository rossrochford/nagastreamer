package server;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.acl.Owner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import messages.StartStreamW;
import util.Constants;

public class UnitManager
{
	static HashMap<Integer, LocalUnit> localUnits; //might be easier to have one collection of units and one class, with an isLocal boolean
	static HashMap<Integer, RemoteUnit> remoteUnits;
	
	public static void loadSettings()
	{
		//TODO: should open a file (whose path is in Constants) and read in config data for the units (their id, num channels, controller info etc) - though maybe in the future this could be a query to the database?
	}
	
//	public static synchronized Unit getForBounceDown(int unitId)
//	{
//		LocalUnit u = localUnits.get(unitId); //LocalUnit u = _getLocalUnitIfAvailable(unitId);
//		if(u == null){
//			System.out.println("UnitManager.getForBounce(): No unit "+unitId+" found in localUnits");
//			return null;
//		}
//		
//		boolean success = u.lockForBounce();
//		if(!success)
//			return null;		
//		
//		return u;
//	}
//	
//	public static synchronized LocalUnit getForRtStream(int unitId)
//	{
//		LocalUnit u = localUnits.get(unitId); //LocalUnit u = _getLocalUnitIfAvailable(unitId);
//		if(u == null){
//			System.out.println("UnitManager.getForBounce(): No unit "+unitId+" found in localUnits");
//			return null;
//		}
//		
//		boolean success = u.markAsInUse();
//		if(!success)
//			return null;		
//		
//		return u;
//	}
//	
//	public static synchronized void markAsAvailable(int unitId)
//	{
//		LocalUnit u = localUnits.get(unitId); //LocalUnit u = _getLocalUnitIfAvailable(unitId);
//		if(u == null){
//			System.out.println("UnitManager.getForBounce(): No unit "+unitId+" found in localUnits");
//			return;
//		}
//		
//		u.markAvailable();
//	}
	
	public static RemoteUnit getRemoteUnit(int unitId)
	{
		return remoteUnits.get(unitId);
	}
	
	public static LocalUnit getLocalUnit(int unitId)
	{
		return localUnits.get(unitId);
	}
	
	public static Unit getUnit(int unitId)
	{
		if(localUnits == null || remoteUnits == null){
			System.out.println("error: UnitManager.loadSettings() should have been called on startup");
			return null;
		}
		
		if(localUnits.get(unitId) != null)
			return localUnits.get(unitId);
		return remoteUnits.get(unitId);
	}
	
	public abstract static class Unit
	{	
		public abstract InetSocketAddress getSocketAddr();
	}
	
	public static class LocalUnit extends Unit
	{
		public static final int AVAILABLE = 0;
		public static final int IN_USE = 1; //e.g. for RtStream
		public static final int LOCKED_FOR_BOUNCE = 2;
		
		int id;
		public int[] channels;
		volatile int availability;
		RtStream owningStream;
		//reference to stream using it currently?
		
		public LocalUnit(int id, int[] channels){
			this.id = id;
			this.channels = channels;
			this.availability = AVAILABLE;
		}
		
		public boolean acquireForStream(int streamType)
		{
			if(streamType == StartStreamW.RT_STREAM)
				return acquireForRtStream();
			if(streamType == StartStreamW.BOUNCE_STREAM)
				return acquireAndLockForBounce();
			
			System.out.println("Invalid stream type");
			return false;
		}
		
		public synchronized boolean acquireAndLockForBounce()
		{
			if(availability == LOCKED_FOR_BOUNCE){
				System.out.println("Unit "+id+" currently occupied with bounce");
				return false;
			}
			
			if(owningStream != null){
				owningStream.notifyUnitRevoked();
			}			
				
			availability = LOCKED_FOR_BOUNCE;
			return true;
		}
		
		public synchronized boolean acquireForRtStream()
		{
			if(availability == LOCKED_FOR_BOUNCE){
				System.out.println("Unit "+id+" currently occupied with bounce");
				return false;
			}			
			availability = IN_USE;
			return true;
		}
		
		public void setOwner(RtStream owningStream)
		{
			this.owningStream = owningStream;
		}
		
		public synchronized boolean acquireForRtStream(RtStream owningStream)
		{
			if(availability == LOCKED_FOR_BOUNCE){
				System.out.println("Unit "+id+" currently occupied with bounce");
				return false;
			}			
			availability = IN_USE;
			this.owningStream = owningStream;
			return true;
		}
		
		public synchronized void markAvailable()
		{
			availability = AVAILABLE;
			
			owningStream = null;
		}
		
		public InetSocketAddress getSocketAddr()
		{
			return new InetSocketAddress("127.0.0.1", Constants.listenPort);
		}
	}
	
	public static class RemoteUnit extends Unit
	{
		String ipAddress;
		
		public RemoteUnit(String ip){
			this.ipAddress = ip;
		}
		
		public InetSocketAddress getSocketAddr()
		{
			return new InetSocketAddress(ipAddress, Constants.listenPort);
		}
	}
}
