package server;

import serialisers.PCM16BitEncoder;
import serialisers.SampleDeserialiser;
import serialisers.SampleSerialiser;
import util.Util;
import workerloops.AudioChunkSender;
import workerloops.AudioChunkSender.AudioChunkSendTask;
import messages.RtAudioChunkW;
import messages.StartStreamW;
import audio.AudioUtil;
import audio.MultiVec;

public class VecSender 
{
	static int buffSize = 3;
	
	CloudioConnection connection;
	int chunkNum = 0;
	float[][][] outputBuffer;
	float[][] currChunkSamps;
	int vecSize;
	int chunkPos = 0;
	SampleSerialiser serialiser;
	StreamSettings ss;
	
	public VecSender(CloudioConnection connection, StreamSettings streamSettings, int vecSize)
	{
		this.connection = connection;
		this.ss = streamSettings;
		
		this.serialiser = SampleSerialiser.getEncoder(ss.compType, ss.numChannels, ss.chunkSize);
		this.vecSize = vecSize;
		
		outputBuffer = new float[buffSize][ss.numChannels][ss.chunkSize];
		currChunkSamps = outputBuffer[chunkNum%buffSize];
	}
	
//	public VecSender(AudioConnection connection, int chunkSize, int numChannels, int vecSize, int streamId, int compType, int bitRate)
//	{
//		this.connection = connection;
//		
//		if(compType == StartStreamW.PCM_16Bit)
//			this.serialiser = new PCM16BitEncoder(); // TODO: add others
//		
//		this.numChannels = numChannels;
//		this.vecSize = vecSize;
//		this.chunkSize = chunkSize;
//		this.streamId = streamId;
//		
//		outputBuffer = new float[buffSize][numChannels][vecSize];
//		currChunkSamps = outputBuffer[chunkNum%buffSize];
//	}
	
	public void sendVec(MultiVec vecs)
	{
		if(ss.chunkSize-chunkPos >= vecSize) //for efficiency, when the number of samples left in currChunkSamps < vecSize, we can do a simple copy 
		{
			AudioUtil.multiArrayCopy(vecs.vecs, 0, currChunkSamps, chunkPos, vecSize);
			chunkPos += vecSize;
			
			if(chunkPos == ss.chunkSize){
				sendChunk();
				chunkPos = 0;
			}
		}
		else
		{
			for(int vecPos=0; vecPos<vecSize; vecPos++) //TODO: make more efficient 
			{
				for(int chan=0; chan < ss.numChannels; chan++)
					currChunkSamps[chan][chunkPos] = vecs.vecs[chan][vecPos];						
				
				chunkPos++;
				
				if(chunkPos == ss.chunkSize){
					sendChunk();
					chunkPos = 0;
				}
			}
		}
	}
	
	public void sendChunk()
	{
		//create chunk and send register it with the send loop
		RtAudioChunkW chunk = new RtAudioChunkW(chunkNum, currChunkSamps, null);
		
		
		
		AudioChunkSendTask task = new AudioChunkSender.AudioChunkSendTask(connection, chunk, serialiser);
		AudioChunkSender.getInstance().registerTask(task, true);
		
		chunkNum++;
		currChunkSamps = outputBuffer[chunkNum%buffSize];		
	}
}
