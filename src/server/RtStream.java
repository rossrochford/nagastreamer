package server;

import java.nio.Buffer;
import java.util.List;

import server.UnitManager.LocalUnit;
import server.UnitManager.Unit;
import util.Constants;
import util.Util;
import workerloops.AsyncExecutor;
import messages.RtAudioChunkW;
import messages.StartStreamW;
import audio.AudioBuffer;
import audio.CircularAudioBuffer2;
import audio.LongAudioBuffer;
import audio.AudioLoop;
import audio.LongAudioBuffer;
import audio.MultiVec;
import audio.Performable;
import audio.RtStreamPlayer2;

public class RtStream implements Performable
{
	CloudioConnection connection;
	StreamSettings streamSettings;
	RtStreamPlayer2 streamPlayer;
	VecSender vecSender;
	LocalUnit currentUnit;
	public AudioBuffer receiveBuffer;
	public int type;
	
	private RtStream(CloudioConnection connection, StreamSettings streamSettings, LocalUnit unit) //streamType?
	{
		this.connection = connection;
		this.streamSettings = streamSettings;
		this.receiveBuffer = new CircularAudioBuffer2(20, streamSettings); //TODO: perhaps change size depending on stream type (for bouncing we need more reliability)
		
		//this.streamPlayer = new RtStreamPlayer2((CircularAudioBuffer2)receiveBuffer, streamSettings, AudioLoop.VEC_SIZE);  //TODO: remove cast
		//this.vecSender = new VecSender(connection, streamSettings, AudioLoop.VEC_SIZE);
		
		this.currentUnit = unit; //UnitManager.getLocalUnit(unitIds.get(0)); //first unit in the list should be local and already acquired (i.e. without an owner)
		
		//unit.registerOwningPerformable(this);
	}
	
	public static RtStream create(CloudioConnection connection, final StreamSettings streamSettings, LocalUnit unit)
	{
		final RtStream newStream = new RtStream(connection, streamSettings, unit);
		//newStream.currentUnit.setOwner(newStream);  lets not worry about units being "owned" or "revoked" for now
		
		AsyncExecutor.getInstance().submit(new Runnable(){
			public void run(){
				newStream._createPlayerAndSender();
			}
		});
		
		return newStream;
	}
	
	public void _createPlayerAndSender() //we do this asynchronously so that RtStream objects can be created in the receive thread
	{
		System.out.println("createPlayerAndSender() called");
		this.streamPlayer = new RtStreamPlayer2((CircularAudioBuffer2)receiveBuffer, streamSettings, AudioLoop.VEC_SIZE); // TODO: remove cast
		this.vecSender = new VecSender(connection, streamSettings, AudioLoop.VEC_SIZE);
	}

	public void receiveChunk(RtAudioChunkW chunkMsg) 
	{
		receiveBuffer.addChunk(chunkMsg);		
	}	
	
	//public void notifyUnitRevoked()
	
	public void startStream()
	{
		if(streamPlayer != null && vecSender != null){
			AudioLoop.getInstance().register(this);
			connection.sendMsg(nextStartStreamMsg(msg));  //should be possible to derive this from RtStream's ivars and possibly additional parameters added to startStream(). Perhaps a simple whose unitIndex gets incremented on each hop.
		}
		else{
			//rare case, but it must be taken care of
			
			//TODO: create async task to start stream (by the time it executes, because of there is only one queue, the streamPlayer and vecSender should have been created) 
		}
	}
	
	/*private static StartStreamW nextStartStreamMsg(StartStreamW msg)
	{
		if(msg.unitIndex == msg.unitIds.size()-1)
			msg.unitIndex = 0;
		else if(msg.unitIndex >= 0 && msg.unitIndex < msg.unitIds.size()-1){
			msg = new StartStreamW(msg.streamType, msg.unitIds, msg.unitIndex+1, msg.sessionUuid, StreamSettings.getLANstreamSettings(msg.streamType, msg.numChannels));
		}
		//else: should never get here because of prior validation
			
		return msg;			
	}*/
	
	public void endStream()
	{
		//should initiate fadeout of vec sender and perform() should deregister the stream once the fadeout has completed
		
		//There should probably be 3 possible states: ACTIVE, ENDING, and FINISHED
	}
	
	public boolean isActive()
	{
		return false;
	}

	public void perform(float[][] in, float[][] out) 
	{	
		//if(Constants.DISABLE_GC)
			//Util.keepFromGC(in); Util.keepFromGC(out);
		
		vecSender.sendVec(new MultiVec(in, currentUnit.channels));
		
		streamPlayer.playVec(new MultiVec(out, currentUnit.channels));
	}
}