package server;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import naga.NIOServerSocket;
import naga.NIOService;
import naga.NIOSocket;
import naga.ServerSocketObserverAdapter;


public class StreamerServer 
{
	HashMap<String, ClientContext> clientContexts;
	
	public StreamerServer()
	{
		clientContexts = new HashMap<String, ClientContext>();
	}
	
	public void start(int port) throws IOException
	{
		NIOService service = new NIOService();
		NIOServerSocket socket = service.openServerSocket(port);
		
		socket.listen(new ServerSocketObserverAdapter()
		{
			public void newConnection(NIOSocket nioSocket)
			{
				System.out.println("Client " + nioSocket.getIp() + " connected.");
				
				nioSocket.listen(new ServerConnectionListener(clientContexts));
			}
		});
	}

}
