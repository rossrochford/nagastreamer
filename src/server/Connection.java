package server;

import messages.SerialisableMessage;

public interface Connection 
{
	public void sendMsg(SerialisableMessage msg);
	
	public void write(byte[] bytes);
}
