package tests;

import java.util.LinkedList;

import org.jitsi.impl.neomedia.pulseaudio.PA.context_success_cb_t;

import messages.RtAudioChunkW;
import messages.StartStreamW;
import serialisers.OpusDecoder;
import serialisers.OpusEncoder;
import server.RtStream;
import server.StreamSettings;
import server.UnitManager;
import server.UnitManager.LocalUnit;
import util.Util;
import workerloops.DecompressionWorker;
import audio.AudioLoop;

public class PerformanceTests 
{
	public static void decompressionTest()
	{
		int numChannels = 2;
		int chunkSize = 960;
		int buffSampLen = 15*AudioLoop.SAMPLE_RATE;
		
		//float[][] sineWave = TestUtil.createSineWave(300, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen);
		OpusEncoder enc = new OpusEncoder(numChannels, AudioLoop.SAMPLE_RATE, 96000, chunkSize);
		LinkedList<RtAudioChunkW> chunks = TestUtil.createSineWaveChunks(300, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen, chunkSize, enc); 
		
		OpusDecoder dec = new OpusDecoder(numChannels, AudioLoop.SAMPLE_RATE, chunkSize);
		long startTime = System.currentTimeMillis();
		for(RtAudioChunkW currChunk : chunks)
			currChunk.samples = dec.deserialiseSamples(currChunk.audioPayload);					
		long endTime = System.currentTimeMillis();
		
		double avgDecompressTime = (endTime-startTime+0.0)/chunks.size();
		
		System.out.println("Average decompression time: "+avgDecompressTime);
	}
	
	public static void compressionTest()
	{
		int numChannels = 2;
		int chunkSize = 960;
		int buffSampLen = 15*AudioLoop.SAMPLE_RATE;
		
		LinkedList<RtAudioChunkW> chunks = TestUtil.createSineWaveChunks(300, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen, chunkSize, null); //TestUtil.createChunks(sineWave, chunkSize);
		
		OpusEncoder enc = new OpusEncoder(numChannels, AudioLoop.SAMPLE_RATE, 96000, chunkSize);
		
		long startTime = System.currentTimeMillis();
		for(RtAudioChunkW currChunk : chunks)
			currChunk.audioPayload = enc.serializeSamples(currChunk.samples);
		long endTime = System.currentTimeMillis();
		
		double avgCompressTime = (endTime-startTime+0.0)/chunks.size();
		
		System.out.println("Average compression time: "+avgCompressTime);
	}
	
	public static void streamCreationTest()
	{		
		int NUM_ITERATIONS = 300000;
		long startTime = System.currentTimeMillis();
		long lastIterTime = System.currentTimeMillis();
		long maxIterTime = 0; long minIterTime = 0;
		StreamSettings ss = new StreamSettings(StartStreamW.RT_STREAM, 2, 48000, 960, StartStreamW.OPUS, 96000);
		for(int i=0; i<NUM_ITERATIONS; i++) //TODO: measure max and min time
		{
			RtStream stream = RtStream.create(StartStreamW.RT_STREAM, null, ss, new LocalUnit(234, new int[]{0,1}));
			
			if(i < 2000)
				continue; //ignoring warm-up iterations
			
			long currTime = System.currentTimeMillis();
			long iterDur = currTime-lastIterTime;
			if(iterDur > maxIterTime)
				maxIterTime = iterDur;
			if(minIterTime == 0 || iterDur < minIterTime)
				minIterTime = iterDur;
			
			lastIterTime = currTime;
		}
		long endTime = System.currentTimeMillis();
		
		System.out.println("Average stream creation time: "+((endTime-startTime+0.0)/NUM_ITERATIONS));
		System.out.println("Max time: "+maxIterTime);
		System.out.println("Min time: "+minIterTime);
	}
	
	public static void threaded_ComDecomp_Test()
	{
		Runnable r1 = new Runnable() {
			public void run() {
				PerformanceTests.decompressionTest(); 				
			}
		};
		
		Runnable r2 = new Runnable() {
			public void run() {
				PerformanceTests.compressionTest(); 				
			}
		};
		
		//PerformanceTests.decompressionTest(); //average decompression time: 0.13 ms
		//PerformanceTests.compressionTest(); //average compression time: 0.13 ms
		
		Util.sleep(2000);
		
		//TODO: create test that allows us to compare the max, min and average timings when there a N number of threads, so we can find a sweet-spot
		
		new Thread(r1).start(); new Thread(r1).start(); new Thread(r1).start();
		
		new Thread(r2).start(); new Thread(r2).start();	new Thread(r2).start();
	}
	
	public static void main(String[] args)
	{
		streamCreationTest();
	}
}
