package tests;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import messages.RtAudioChunkW;
import serialisers.OpusDecoder;
import serialisers.OpusEncoder;
import serialisers.SampleDeserialiser;
import serialisers.SampleSerialiser;
import util.Util;
import audio.AudioUtil;
import be.hogent.tarsos.dsp.pitch.DynamicWavelet;
import be.hogent.tarsos.dsp.pitch.FastYin;
import be.hogent.tarsos.dsp.pitch.McLeodPitchMethod;
import be.hogent.tarsos.dsp.pitch.PitchDetectionResult;
import be.hogent.tarsos.dsp.pitch.PitchDetector;
import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;

public class TestUtil 
{
	/*
	public static float[] createSineWave(double freq, int length) //TODO: shouldn't this adjust depend on the sample rate?
    {
        float[] sines = new float[length];
        for (int i = 0; i < length; i++)
        {
            double radians = (Math.PI/freq) * i;
            sines[i] = (float) Math.sin(radians);
        }
         
         return sines;
    }
	
	public static float[] createSineWave2(double freq, int length) //TODO: shouldn't this adjust depend on the sample rate?
    {
        float[] sines = new float[length];
        for (int i = 0; i < length; i++)
        	sines[i] = (float) Math.sin(2*Math.PI*(freq)*i);
         
         return sines;
    }*/
	
	public static float[][] sineSeg(int pos, int freq, int numChannels, double sampleRate, int length)
	{
		int ctSamplesTotal = length;
		double fCycleInc = freq/sampleRate;  // Fraction of cycle between samples
		
		float[] samps = new float[length];
		double fCyclePosition = 0; 
		
		for(int i=0; i<pos; i++)
		{
			fCyclePosition += fCycleInc;
            if (fCyclePosition > 1)
            	fCyclePosition -= 1;
		}
		
		for(int i=0; i<length; i++)
		{
			samps[i] = (float) Math.sin(2*Math.PI * fCyclePosition);
			
			fCyclePosition += fCycleInc;
            if (fCyclePosition > 1)
            	fCyclePosition -= 1;
		}
		
		float[][] ans = new float[numChannels][length];
		
		ans[0] = samps;
		for(int chan=1; chan<numChannels; chan++)
			ans[chan] = samps; //samps.clone(); 
		
		return ans;
	}
	
	public static float[][] createSineWave(double freq, int numChannels, double sampleRate, int length)
	{
		int ctSamplesTotal = length;
		double fCycleInc = freq/sampleRate;  // Fraction of cycle between samples
		
		float[] samps = new float[length];
		double fCyclePosition = 0; 
		
		for(int i=0; i<length; i++)
		{
			samps[i] = (float) Math.sin(2*Math.PI * fCyclePosition);
			
			fCyclePosition += fCycleInc;
            if (fCyclePosition > 1)
            	fCyclePosition -= 1;
		}
		
		float[][] ans = new float[numChannels][length];
		
		for(int chan=0; chan<numChannels; chan++)
			ans[chan] = samps.clone();
		
		return ans;
	}
	
	public static float[][] chunkAndEncDec(float[][] samps, SampleSerialiser enc, SampleDeserialiser dec)
	{
		int chunkSize = 960;
		//int numSamps = (int) sampleRate*3; //3 seconds
		int numChannels = samps.length;
		int len = samps[0].length;	
		
		float[][] decSamps = new float[numChannels][len];
		
		//OpusEncoder enc = new OpusEncoder(numChannels, sampleRate, bitRate, chunkSize);
		//OpusDecoder dec = new OpusDecoder(numChannels, sampleRate, chunkSize);
		
		for(int i=0; i+chunkSize<len; i+=chunkSize)
		{			
			float[][] chunk = AudioUtil.seg(samps, i, chunkSize);
			
			byte[] compBytes = enc.serializeSamples(chunk);
			float[][] decChunkSamps = dec.deserialiseSamples(compBytes);
			
			for(int chan=0; chan<numChannels; chan++)
				System.arraycopy(decChunkSamps[chan], 0, decSamps[chan], i, chunkSize);
		}
		
		return decSamps;
	}
	
	public static ArrayList<RtAudioChunkW> createSineWaveChunks(int freq, int numChannels, int sampleRate, int len, int chunkSize, SampleSerialiser enc)
	{
		return _createSineWaveChunks(freq, numChannels, sampleRate, len, chunkSize, enc, false);
	}
	
	public static ArrayList<RtAudioChunkW> createNoisySineWaveChunks(int freq, int numChannels, int sampleRate, int len, int chunkSize, SampleSerialiser enc)
	{
		return _createSineWaveChunks(freq, numChannels, sampleRate, len, chunkSize, enc, true);
	}
	
	private static ArrayList<RtAudioChunkW> _createSineWaveChunks(int freq, int numChannels, int sampleRate, int len, int chunkSize, SampleSerialiser enc, boolean isNoisy)
	{		
//		int numChannels = samps.length;
//		int len = samps[0].length;	
		
		//float[][] decSamps = new float[numChannels][len];
		
		ArrayList<RtAudioChunkW> chunks = new ArrayList<RtAudioChunkW>();
	
		int chunkNum = 0;
		
		for(int i=0; i+chunkSize<len; i+=chunkSize)
		{	
//			if(isNoisy)
//				freq = freq + ((Math.random()*200)-100);
			
			float[][] chunkSamps = sineSeg(i, freq, numChannels, sampleRate, chunkSize); //AudioUtil.seg(samps, i, chunkSize);
			
//			if(isNoisy)
//				for(int chan=0;chan<chunkSamps.length;chan++)
//					for(int j=0; j<chunkSamps[0].length; j++)
//						chunkSamps[chan][j] += ((Math.random()*0.4)-0.2);
			
			RtAudioChunkW chunk = new RtAudioChunkW(chunkNum, chunkSamps, null);
			
			if(enc != null){
				chunk.serialiseSamples(enc);
				chunk.samples = null;  //we no longer need these in our test data
			}
			
			chunks.add(chunk);	
			chunkNum++;
		}
		
		return chunks;
	}
	
	public static LinkedList<RtAudioChunkW> createChunks(float[][] samps, int chunkSize)
	{
		return createChunks(samps, chunkSize, null);
	}
	
	public static LinkedList<RtAudioChunkW> createChunks(float[][] samps, int chunkSize, SampleSerialiser enc)
	{		
		int numChannels = samps.length;
		int len = samps[0].length;	
		
		//float[][] decSamps = new float[numChannels][len];
		
		LinkedList<RtAudioChunkW> chunks = new LinkedList<RtAudioChunkW>();
		
		int chunkNum = 0;
		
		for(int i=0; i+chunkSize<len; i+=chunkSize)
		{			
			float[][] chunkSamps = AudioUtil.seg(samps, i, chunkSize);
			
			RtAudioChunkW chunk = new RtAudioChunkW(chunkNum, chunkSamps, null);
			
			if(enc != null)
				chunk.audioPayload = enc.serializeSamples(chunk.samples);				
			
			chunks.add(chunk);	
			chunkNum++;
			
			System.gc();
		}
		
		return chunks;
	}
	
	public static boolean isSineShape(float[][] samps, double expectedFreq)
	{
		int windowSize = 3;
		double expectedAbsAngle;
		
		if(expectedFreq <= 800)
			expectedAbsAngle = Util.scale(expectedFreq, 50, 800, 0.00654, 0.10414);
		else if(expectedFreq <= 1600)
			expectedAbsAngle = Util.scale(expectedFreq, 800, 1600, 0.10414, 0.2048);
		else if(expectedFreq <= 3000)
			expectedAbsAngle = Util.scale(expectedFreq, 1600, 3000, 0.2048, 0.3632);
		else{
			System.out.println("warning: isSineShare(samps, expectedFreq) only supports frequencies between 50 and 3000 hz");
			return isSineShape(samps);
		}
		
		//exp = expectedAbsAngle;
		
		double tolerance = 0.02; //for freqs between 50 and 3000, a perfect sine wave was found to be within 0.007849 which simply reflects the errors in the linear scaling
		
		double lowerAngleThresh = expectedAbsAngle-tolerance;
		double higherAngleThresh = expectedAbsAngle+tolerance;
		
		return _isSineShape(samps, lowerAngleThresh, higherAngleThresh, windowSize);
	}
	
	public static double actual = 0.0;
	public static double exp = 0.0;
	private static void findTolerance(String[] args) //for manually testing our angle tolerance
	{
		 double maxDiff = 0.0;
		 for(int freq=50; freq<3000; freq++)
		 {
			 float[][] sineSamps = createSineWave(freq, 1, 48000, 9000);
			 isSineShape(sineSamps, freq);
			 
			 double diff = Math.abs(exp-actual);
			 if(diff > maxDiff)
				 maxDiff = diff;
			 
			 System.out.println("---------------------------freq: "+freq);
		 }
		 
		 System.out.println("maxDiff: "+maxDiff);	//the tolernce should be something like this	 
	 }
	
	public static boolean isSineShape(float[][] samps)
	{	//without knowing the frequency, we'll guess that its between 50Hz and 5000Hz. When analysed with a window size of 3, the sine wave will have an abs angle between 0.0065 and 0.55. (we assume a sample rate of 48000 here)
		return _isSineShape(samps, 0.0065, 0.55, 3);
	}
	
	private static boolean _isSineShape(float[][] samps, double lowerAngleThresh, double higherAngleThresh, int windowSize)
	{
		//int windowSize = 3;
		double minAngle = 0.0; 
		double maxAngle = 0.0;
		for(int chan=0; chan<samps.length; chan++)
		{
			for(int i=windowSize; i<samps[0].length; i++)
			{
				double currAngle = (samps[chan][i-windowSize] - samps[chan][i])/windowSize;
				
				if(currAngle < minAngle)
					minAngle = currAngle;
				if(currAngle > maxAngle)
					maxAngle = currAngle;
			}
		}
		
		//actual = maxAngle;		
		
		minAngle = Math.abs(minAngle);
		if(Math.abs(minAngle-maxAngle) > 0.03)
			return false;
		if(maxAngle < lowerAngleThresh || maxAngle > higherAngleThresh) 
			return false; //assuming frequence between 50-5000Hz and a sample rate of 48000
		return true;		
	}
	
	/*
	 public static double[] fft(double[] input)
	 {
		 DoubleFFT_1D fftDo = new DoubleFFT_1D(input.length);
		 double[] fft = new double[input.length * 2];
		 System.arraycopy(input, 0, fft, 0, input.length);
		 fftDo.complexForward(fft);
		 return fft;
	 }*/
	 
	 public static double detectPitch(float[][] samples, double sampleRate)
	 {
		 //PitchDetector p = new FastYin((float) sampleRate, 2048);
		 //PitchDetector p = new McLeodPitchMethod((float) sampleRate, 4096);
		 PitchDetector p = new DynamicWavelet((float) sampleRate, 2048);
		 PitchDetectionResult res = p.getPitch(samples[0]); //we'll just use the first channel
		 
		 return res.getPitch();
	 }
	 
	 public static void writeObject(Object obj, String filepath)
	 {
		FileOutputStream fos = null;
	    ObjectOutputStream out = null;
	    try {
	      fos = new FileOutputStream(filepath);
	      out = new ObjectOutputStream(fos);
	      out.writeObject(obj);

	      out.close();
	    } 
	    catch (Exception ex) {ex.printStackTrace();}
	 }
	 
	 public static Object readObject(String filepath)
	 {
		 Object obj = null;
		 FileInputStream fis = null;
	    ObjectInputStream in = null;
	    try {
	      fis = new FileInputStream(filepath);
	      in = new ObjectInputStream(fis);
	      obj = in.readObject();
	      in.close();
	    } 
	    catch (Exception ex) {ex.printStackTrace();}
		    
	    return obj;
	}
	 
	 /*
	 public static double detectPitch2(float[] samples)
	 {
		 int bufferSize = 2048;
		 DoubleFFT_1D fft1d = new DoubleFFT_1D(bufferSize);
		 double[] fftBuffer = new double[bufferSize*2];
		 double[] magnitude = new double[bufferSize/2];
		 // copy real input data to complex FFT buffer
		 for(int i = 0; i < bufferSize-1; ++i){
			 fftBuffer[2*i] = samples[i];
			 fftBuffer[2*i+1] = 0;
		 }
		 //perform FFT on fft[] buffer
		 fft1d.realForward(fftBuffer);
		 //fft1d.complexForward(fftBuffer);
		 // calculate power spectrum (magnitude) values from fft[]
		 for(int i = 0; i < (bufferSize/2)-1; ++i) {
			 double real =  fftBuffer[2*i];
	         double imaginary =  fftBuffer[2*i + 1];
			 //double real = (2 * fftBuffer[i]);
			 //double imaginary = (2 * fftBuffer[i] + 1);
			 magnitude[i] = Math.sqrt( (real*real) + (imaginary*imaginary) );
		 }
		 double maxVal = 0;
		 int binNo = 0;
		 // find largest peak in power spectrum
		 for(int i = 0; i < (bufferSize/2)-1; ++i) {
			 if(magnitude[i] > maxVal){
				 maxVal = (int) magnitude[i];
				 binNo = i;
			 }
		 }
		 // convert index of largest peak to frequency
		 double freq = (48000.0 * binNo)/bufferSize;
		 
		 System.out.println("binNo: "+binNo);
		 
		 return freq;
	 }
	 
	 public static double detectPitch(float[] samples) //note: samples.length determines window size
	 {
		 double[] doubleSamples = new double[samples.length];
		 for(int i=0; i<samples.length; i++)
			 doubleSamples[i] = samples[i]; 
		 
		 double[] fft = fft(doubleSamples);
		 double max = 0.0;
		 int maxPos = 0;
		
		 for(int i=0; i<fft.length/2; i++)
		 {
			 double mag = Math.sqrt((fft[2*i]*fft[2*i])+(fft[(2*i)+1]*fft[(2*i)+1]));
			 
			 if(mag > max){
				 max = mag;
				 maxPos = i;
			 }				 
		 }
		 
		 int fftWindowSize = samples.length;
		 double freq = (48000.0*maxPos) / fftWindowSize;
		 System.out.println("maxPos: "+maxPos);
		 return freq;
	 }*/
	 
//	 public static void main(String[] args)
//	 {
//		 float[][] sineSamps = createSineWave(675, 1, 48000, 9000); //(double freq, int numChannels, double sampleRate, int length)
//		 
//		 double pitch = detectPitch(sineSamps, 48000);
//		 
//		 System.out.println(pitch);
//	 }	 
}
