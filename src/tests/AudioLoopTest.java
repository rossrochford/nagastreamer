package tests;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.sound.sampled.Clip;

import net.beadsproject.beads.ugens.WavePlayer;

import com.ritolaaudio.simplewavio.WaveIOUtils;

import ddf.minim.Minim;
import ddf.minim.MultiChannelBuffer;
import messages.RtAudioChunkW;
import messages.StartStreamW;
import serialisers.OpusDecoder;
import serialisers.OpusEncoder;
import serialisers.SampleDeserialiser;
import serialisers.SampleSerialiser;
import server.StreamSettings;
import util.EditableFloat;
import util.Util;
import workerloops.ClockedRunnable;
import workerloops.DecompressionWorker;
import audio.AudioBuffer;
import audio.CircularAudioBuffer;
import audio.CircularAudioBuffer2;
import audio.LongAudioBuffer;
import audio.AudioLoop;
import audio.AudioUtil;
import audio.LongAudioBuffer;
import audio.MultiVec;
import audio.Performable;
import audio.RtStreamPlayer;
import audio.RtStreamPlayer2;

public class AudioLoopTest 
{
	
	public abstract static class PlayerPerformer implements Performable //plays audio in a buffer on loop
	{
		
		protected float[][] waveSamps;
		private int pos = 0;
		
		public void perform(float[][] in, float[][] out) {
			
			if(waveSamps == null)
				System.out.println("Error: waveSamps is null");
			
			int numChannels = waveSamps.length;
			
			for(int chan=0;chan<numChannels;chan++)
				System.arraycopy(waveSamps[chan], pos, out[chan], 0, AudioLoop.VEC_SIZE);		
			
			pos += AudioLoop.VEC_SIZE;
			if(pos+AudioLoop.VEC_SIZE >= waveSamps[0].length)
				pos = 0;
		}
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void testInOutPassThrough__with_hardware_loop() //simple test, to be validated aurally
	{
		AudioLoop.HARDWARE = true;
		
		AudioLoop.getInstance().register(new PassThroughPerformable());		
		AudioLoop.getInstance().startAudio();
	}
	
	public static class PassThroughPerformable implements Performable 
	{
		boolean firstPerformCall = true;
		long lastPerformTime = 0;
		
		//simply passes the audio through untouched. This is useful for testing
		public void perform(float[][] in, float[][] out) 
		{
			int vecSize = in[0].length;
			int numChannels = in.length;
			
			if(firstPerformCall){
				System.out.println("vecSize: "+vecSize+" numChannels: "+numChannels);
				firstPerformCall = false;
			}
			
			for(int chan=0; chan < numChannels; chan++)
				System.arraycopy(in[chan], 0, out[chan], 0, vecSize);
		}
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static class SineTestPerformable extends PlayerPerformer
	{		
		public SineTestPerformable(int numChannels)
		{
			waveSamps = TestUtil.createSineWave(300, numChannels, AudioLoop.SAMPLE_RATE, AudioLoop.SAMPLE_RATE*5);
		}	
	}
	
	public static void test_sine_output() //simple test, to be validated aurally
	{
		AudioLoop.HARDWARE = true;
		AudioLoop.ENABLE_STEREO_TEST_MODE = false;
		
		AudioLoop.getInstance().register(new SineTestPerformable(2));		
		AudioLoop.getInstance().startAudio();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static class OpusSerialisationTestPerformable extends PlayerPerformer
	{
		int pos = 0;
		int numChannels;
		
		public OpusSerialisationTestPerformable()
		{
			int chunkSize = 960;
			int bitRate = 96000;
			numChannels = 2; //AudioLoop.TOTAL_CHANNELS;
			
			float[][] originalSineWave = TestUtil.createSineWave(300, numChannels, AudioLoop.SAMPLE_RATE, AudioLoop.SAMPLE_RATE*8);
			
			OpusEncoder enc = new OpusEncoder(numChannels, AudioLoop.SAMPLE_RATE, bitRate, chunkSize);
			OpusDecoder dec = new OpusDecoder(numChannels, AudioLoop.SAMPLE_RATE, chunkSize);
			
			float[][] decSamps = TestUtil.chunkAndEncDec(originalSineWave, enc, dec);
			
			waveSamps = decSamps;
		}
	}
	
	public static void test_opus() //simple test, to be validated aurally
	{
		AudioLoop.HARDWARE = true;
		
		AudioLoop.getInstance().register(new OpusSerialisationTestPerformable());		
		AudioLoop.getInstance().startAudio();
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//TODO: make a copy of this class but where the content of decSamps is loaded from calls to RtPlayer.playVec()
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static class RtPlayerTest_Offline extends PlayerPerformer
	{
		int pos = 0;
		int numChannels;
		
		public RtPlayerTest_Offline()
		{	
			int numChannels = 2;
			int chunkSize = 480;
			int buffSampLen = AudioLoop.SAMPLE_RATE*9;
			//float[][] sineWave = TestUtil.createSineWave(300, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen);
			OpusEncoder enc = new OpusEncoder(numChannels, AudioLoop.SAMPLE_RATE, 96000, chunkSize);
			ArrayList<RtAudioChunkW> chunks = TestUtil.createSineWaveChunks(300, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen, chunkSize, enc); //TestUtil.createChunks(sineWave, chunkSize, enc);
			
			//if(decompressBeforePlayback){
			OpusDecoder dec = new OpusDecoder(numChannels, AudioLoop.SAMPLE_RATE, chunkSize);
			for(RtAudioChunkW currChunk : chunks)
				currChunk.samples = dec.deserialiseSamples(currChunk.audioPayload);					
			
			DecompressionWorker.getInstance().start();

			LongAudioBuffer buf = new LongAudioBuffer(chunks); //<-- here is the main difference, the buffer has already been loaded with chunks

			StreamSettings ss = new StreamSettings(StartStreamW.BOUNCE_STREAM, numChannels, AudioLoop.SAMPLE_RATE, chunkSize, StartStreamW.OPUS, 96000);
			RtStreamPlayer rtPlayer = new RtStreamPlayer(buf, ss, AudioLoop.VEC_SIZE);
			
			waveSamps = new float[numChannels][buffSampLen];
			
			for(int i=0; i+AudioLoop.VEC_SIZE<buffSampLen; i+=AudioLoop.VEC_SIZE){
				float[][] outVec = new float[numChannels][AudioLoop.VEC_SIZE];
				rtPlayer.playVec(new MultiVec(outVec));
				AudioUtil.multiArrayCopy(outVec, 0, waveSamps, i, AudioLoop.VEC_SIZE);
				System.out.println("i: "+i);
			}
		}
	}
	
	public static void test_RtPlayerTest_Offline() //simple test, to be validated aurally
	{
		AudioLoop.HARDWARE = true;
		AudioLoop.ENABLE_STEREO_TEST_MODE = false;
		
		AudioLoop.getInstance().register(new RtPlayerTest_Offline());		
		AudioLoop.getInstance().startAudio();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static class RtPlayerTest_bounce_stream extends PlayerPerformer//implements Performable
	{
		RtStreamPlayer rtPlayer;
		int buffSampLen = AudioLoop.SAMPLE_RATE*5;
		
		public RtPlayerTest_bounce_stream()
		{
			boolean decompressBeforePlayback = false;
			
			int numChannels = 2;
			int chunkSize = 480;
			//float[][] sineWave = TestUtil.createSineWave(300, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen);
			OpusEncoder enc = new OpusEncoder(numChannels, AudioLoop.SAMPLE_RATE, 96000, chunkSize);
			ArrayList<RtAudioChunkW> chunks = TestUtil.createSineWaveChunks(300, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen, chunkSize, enc); //TestUtil.createChunks(sineWave, chunkSize, enc);
			
			if(decompressBeforePlayback){
				OpusDecoder dec = new OpusDecoder(numChannels, AudioLoop.SAMPLE_RATE, chunkSize);
				for(RtAudioChunkW currChunk : chunks)
					currChunk.samples = dec.deserialiseSamples(currChunk.audioPayload);					
			}
			DecompressionWorker.getInstance().start();

			LongAudioBuffer buf = new LongAudioBuffer(chunks); //<-- here is the main difference, the buffer has already been loaded with chunks

			StreamSettings ss = new StreamSettings(StartStreamW.BOUNCE_STREAM, numChannels, AudioLoop.SAMPLE_RATE, chunkSize, StartStreamW.OPUS, 96000);
			rtPlayer = new RtStreamPlayer(buf, ss, AudioLoop.VEC_SIZE);
		}

		public void perform(float[][] in, float[][] out) {
			
			//MultiVec multiVec = new MultiVec(out.length, out[0].length);
			
			rtPlayer.playVec(new MultiVec(out));	
			
			//rtPlayer.playVec(new MultiVec(out));
			
//			for(int chan=0; chan < out.length; chan++)
//				for(int i=0; i<AudioLoop.VEC_SIZE; i++)
//					out[chan][i] = multiVec.vecs[chan][i];//(float) (Math.random()*2.0)-1;
			
			if(rtPlayer.playbackPos >= rtPlayer.bufferSampLen()){
				System.out.println("removing RtPlayerTestPerformable from AudioLoop");
				AudioLoop.getInstance().unregister(this);
			}
			
			DecompressionWorker.getInstance().unblockIfNecessary();
		}		
	}
	
	public static void test_rtplayer_with_bounce_stream()
	{
		AudioLoop.HARDWARE = true;	
		AudioLoop.ENABLE_STEREO_TEST_MODE = false;
		AudioLoop.getInstance().register(new RtPlayerTest_bounce_stream());	
		System.out.println("starting audio");
		AudioLoop.getInstance().startAudio();
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public static class RtPlayerPerformable implements Performable
{
	RtStreamPlayer rtPlayer;
	
	public RtPlayerPerformable(RtStreamPlayer rtPlayer)
	{
		this.rtPlayer = rtPlayer;
	}
	
	public void perform(float[][] in, float[][] out) 
	{		
		rtPlayer.playVec(new MultiVec(out));
		
		DecompressionWorker.getInstance().unblockIfNecessary();
		
		if(rtPlayer.playbackPos >= rtPlayer.bufferSampLen()){
			AudioLoop.getInstance().unregister(this);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
public static class RtPlayerTest_Performable implements Performable
{
	RtStreamPlayer rtPlayer;
	int buffSampLen;
	List<RtAudioChunkW> chunks;
	final int chunkSize = 960; 
	volatile float fadeIncr = (float) (1.0/(AudioLoop.SAMPLE_RATE*1)); //final EditableFloat fadeIncr = new Float(1/(AudioLoop.SAMPLE_RATE*3)); //should fade from silent to full over 3 seconds
	volatile float fadeVal = (float)(Math.random()); //starting at a random fade value
	boolean modulateVolume = false;
	
	public RtPlayerTest_Performable(int lengthSecs, Class BufferType)
	{
		boolean decompressBeforePlayback = false;
		buffSampLen = AudioLoop.SAMPLE_RATE*lengthSecs;
		
		int numChannels = 2;
		//float[][] sineWave = TestUtil.createSineWave(300, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen);
		OpusEncoder enc = new OpusEncoder(numChannels, AudioLoop.SAMPLE_RATE, 128000, chunkSize);
		int freq = (int) Math.round((Math.random()*12)+1)*60;
		
		String cacheFilename = "cached_opus_chunks_"+freq+"Hz_"+lengthSecs+"s";
		if(new File(cacheFilename).exists()){
			chunks = (LinkedList<RtAudioChunkW>) TestUtil.readObject(cacheFilename);
			System.out.println("Loading cached sinewave");
			
			//temporary
			//for(RtAudioChunkW chunk : chunks)
				//chunk.samples = null;
		}
		else{
			chunks = TestUtil.createSineWaveChunks(freq, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen, chunkSize, enc); //TestUtil.createChunks(sineWave, chunkSize, enc);
			TestUtil.writeObject(chunks, cacheFilename);
		}		
		
		if(decompressBeforePlayback){
			OpusDecoder dec = new OpusDecoder(numChannels, AudioLoop.SAMPLE_RATE, chunkSize);
			for(RtAudioChunkW currChunk : chunks)
				currChunk.samples = dec.deserialiseSamples(currChunk.audioPayload, currChunk.audioPayloadOffset);					
		}
		
		DecompressionWorker.getInstance().start();
			
		AudioBuffer buf = null;
		if(BufferType.equals(LongAudioBuffer.class))
			buf = new LongAudioBuffer();
		else if(BufferType.equals(CircularAudioBuffer.class))
			buf = new CircularAudioBuffer();		
														//TEMPORARY making this a bounce stream
		StreamSettings ss = new StreamSettings(StartStreamW.BOUNCE_STREAM, numChannels, AudioLoop.SAMPLE_RATE, chunkSize, StartStreamW.OPUS, 96000);
		rtPlayer = new RtStreamPlayer(buf, ss, AudioLoop.VEC_SIZE);
		
		System.out.println("RtPlayerTest_rt_stream created with frequency: "+freq+" and buf: "+buf);
	}
	
	public void startTest()
	{
		Thread t = new Thread(new ClockedRunnable(Util.sampsToMs(chunkSize)) {
			AudioBuffer buf = rtPlayer.buffer;
			int chunkNum = 0;
			
			Iterator<RtAudioChunkW> iter = chunks.iterator();
			
			public void doStuff() {
				
				if(iter.hasNext()){
					RtAudioChunkW currChunk = iter.next();
					currChunk.timestamp();
					buf.addChunk(currChunk);
				}
				else if(Math.random() < 0.05)
					System.out.println("finished send simulation");				
				
				/*if(chunkNum < chunks.size()){
					chunks.get(chunkNum).timestamp();
					buf.addChunk(chunks.get(chunkNum));
					chunkNum++;
				}
				else 
					if(Math.random() < 0.05)
						System.out.println("finished send simulation");*/
			}
		}); t.start();
		
		//Old: Util.sleep(Math.round(Util.sampsToMs(RtStreamPlayer.INITIAL_PLAYBACK_OFFSET)));

		System.out.println("registering rtPlayerTest");
		AudioLoop.getInstance().register(this);	
	}

	public void perform(float[][] in, float[][] out) 
	{		
		rtPlayer.playVec(new MultiVec(out));
		
		if(modulateVolume)
			for(int i=0; i<out[0].length; i++){ //we're adding a fade in/out to make multiple performers more audible
				for(int chan=0; chan<out.length; chan++)
					out[chan][i] = (float) (out[chan][i]*fadeVal*0.7);
				fadeVal += fadeIncr;
				if(fadeVal <= 0.0 || fadeVal >= 1.0)
					fadeIncr *= -1.0;
			}
		
		DecompressionWorker.getInstance().unblockIfNecessary();
		
		if(rtPlayer.playbackPos >= buffSampLen){
			AudioLoop.getInstance().unregister(this);
			System.out.println("unregistering RtPlayerTest_rt_stream performer");
			}
	}	
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

public static class RtPlayer2Test_Performable implements Performable
{
	final int chunkSize = 960;
	int numChannels = 2;					//TEMPORARY making this a bounce stream		
	final StreamSettings ss = new StreamSettings(StartStreamW.BOUNCE_STREAM, numChannels, AudioLoop.SAMPLE_RATE, chunkSize, StartStreamW.OPUS, 96000);
	boolean decompressBeforePlayback = false;
	
	RtStreamPlayer2 rtPlayer;
	int buffSampLen;
	List<RtAudioChunkW> chunks;
	
	
	public RtPlayer2Test_Performable(int lengthSecs)//, Class BufferType)
	{
		buffSampLen = AudioLoop.SAMPLE_RATE*lengthSecs;
		
		int freq = (int) Math.round((Math.random()*10)+1)*60;
		loadData(freq, lengthSecs, decompressBeforePlayback);
		
		CircularAudioBuffer2 buf = new CircularAudioBuffer2(17, ss);
		rtPlayer = new RtStreamPlayer2(buf, ss, AudioLoop.VEC_SIZE); 
		
		DecompressionWorker.getInstance().start();
		
		System.out.println("RtPlayerTest_rt_stream created with frequency: "+freq+" and buf: "+buf);
	}
	
	public void loadData(int freq, int lengthSecs, boolean decompressBeforePlayback)
	{
		String cacheFilename = "cached_opus_chunks_"+freq+"Hz_"+lengthSecs+"s";
		if(new File(cacheFilename).exists())
			chunks = (ArrayList<RtAudioChunkW>) TestUtil.readObject(cacheFilename);
		else{
			OpusEncoder enc = new OpusEncoder(numChannels, AudioLoop.SAMPLE_RATE, 128000, chunkSize);
			chunks = TestUtil.createSineWaveChunks(freq, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen, chunkSize, enc); //TestUtil.createChunks(sineWave, chunkSize, enc);
			TestUtil.writeObject(chunks, cacheFilename);
		}		
		
		if(decompressBeforePlayback){
			OpusDecoder dec = new OpusDecoder(numChannels, AudioLoop.SAMPLE_RATE, chunkSize);
			for(RtAudioChunkW currChunk : chunks)
				currChunk.samples = dec.deserialiseSamples(currChunk.audioPayload);					
		}
	}
	
	public void startTest()
	{
		Thread t = new Thread(new ClockedRunnable(Util.sampsToMs(chunkSize)) {
			CircularAudioBuffer2 buf = rtPlayer.buffer;
			int chunkNum = 0;
			
			Iterator<RtAudioChunkW> iter = chunks.iterator();
			
			public void doStuff() {
				
				if(iter.hasNext()){
					RtAudioChunkW currChunk = iter.next();
					currChunk.timestamp();
					buf.addChunk(currChunk);
				}
				else if(Math.random() < 0.005)
					System.out.println("finished send simulation");				
			}
		}); t.start();

		System.out.println("registering rtPlayerTest");
		AudioLoop.getInstance().register(this);	
	}

	public void perform(float[][] in, float[][] out) 
	{		
		rtPlayer.playVec(new MultiVec(out));
		
		DecompressionWorker.getInstance().unblockIfNecessary();
		
		if(rtPlayer.playbackPos >= buffSampLen){
			AudioLoop.getInstance().unregister(this);
			System.out.println("unregistering RtPlayerTest_rt_stream performer");
			}
	}	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

public static class RtPlayerTest_Generic_Performable implements Performable
{
	RtStreamPlayer rtPlayer;
	int buffSampLen;
	List<RtAudioChunkW> chunks;
	final int chunkSize = 960;
	volatile float fadeIncr = (float) (1.0/(AudioLoop.SAMPLE_RATE*1)); //final EditableFloat fadeIncr = new Float(1/(AudioLoop.SAMPLE_RATE*3)); //should fade from silent to full over 3 seconds
	volatile float fadeVal = (float)(Math.random()); //starting at a random fade value
	boolean modulateVolume = false;
	boolean loadBeforePlayback;
	
	public RtPlayerTest_Generic_Performable(int lengthSecs, int freq, Class BufferType, int streamType, boolean loadBefore, boolean decompressBeforePlayback)
	{
		this.loadBeforePlayback = loadBefore;
		
		if((streamType == StartStreamW.BOUNCE_STREAM || loadBefore) && BufferType.equals(CircularAudioBuffer.class)){
			System.out.println("error: bad parameter combination");
			System.exit(0);
		}
		
		buffSampLen = AudioLoop.SAMPLE_RATE*lengthSecs;
		
		int numChannels = 2;
		OpusEncoder enc = new OpusEncoder(numChannels, AudioLoop.SAMPLE_RATE, 128000, chunkSize);
		//int freq = (int) Math.round((Math.random()*12)+1)*60;
		
		String cacheFilename = "cached_opus_chunks_"+freq+"Hz_"+lengthSecs+"s";
		if(!new File(cacheFilename).exists()){
			chunks = TestUtil.createSineWaveChunks(freq, numChannels, AudioLoop.SAMPLE_RATE, buffSampLen, chunkSize, enc); //TestUtil.createChunks(sineWave, chunkSize, enc);
			TestUtil.writeObject(chunks, cacheFilename);
		}
		chunks = (ArrayList<RtAudioChunkW>) TestUtil.readObject(cacheFilename);				
		
		if(decompressBeforePlayback){
			OpusDecoder dec = new OpusDecoder(numChannels, AudioLoop.SAMPLE_RATE, chunkSize);
			for(RtAudioChunkW currChunk : chunks)
				currChunk.samples = dec.deserialiseSamples(currChunk.audioPayload);					
		}
		
		DecompressionWorker.getInstance().start();
		
		AudioBuffer buf = (BufferType.equals(LongAudioBuffer.class)) ? new LongAudioBuffer() : new CircularAudioBuffer();

		StreamSettings ss = new StreamSettings(streamType, numChannels, AudioLoop.SAMPLE_RATE, chunkSize, StartStreamW.OPUS, 96000);
		rtPlayer = new RtStreamPlayer(buf, ss, AudioLoop.VEC_SIZE);
		
		if(loadBefore){
			for(int chunkNum=0; chunkNum<chunks.size(); chunkNum++){
				chunks.get(chunkNum).arrivalTime = (int) Math.round(chunkNum*Util.sampsToMs(chunkSize)); //simulate time intervals
				rtPlayer.buffer.addChunk(chunks.get(chunkNum));
			}
		}
	}
	
	public void startTest()
	{
		if(!loadBeforePlayback){
			Thread t = new Thread(new ClockedRunnable(Util.sampsToMs(chunkSize)) {
				AudioBuffer buf = rtPlayer.buffer;
				int chunkNum = 0;
				
				public void doStuff() {
					if(chunkNum < chunks.size()){
						chunks.get(chunkNum).timestamp();
						buf.addChunk(chunks.get(chunkNum));
						chunkNum++;
					}
				}
			}); t.start();
		}

		AudioLoop.getInstance().register(this);	
	}

	public void perform(float[][] in, float[][] out) 
	{		
		rtPlayer.playVec(new MultiVec(out));
		
		if(modulateVolume)
			for(int i=0; i<out[0].length; i++){ //we're adding a fade in/out to make multiple performers more audible
				for(int chan=0; chan<out.length; chan++)
					out[chan][i] = out[chan][i]*fadeVal;
				fadeVal += fadeIncr;
				if(fadeVal <= 0.0 || fadeVal >= 1.0)
					fadeIncr *= -1.0;
			}
		
		DecompressionWorker.getInstance().unblockIfNecessary();
		
		if(rtPlayer.playbackPos >= buffSampLen){
			AudioLoop.getInstance().unregister(this);
			System.out.println("unregistering RtPlayerTest_rt_stream performer");
			}
	}		
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static class FilePlayer extends PlayerPerformer
	{		
		public FilePlayer(String filepath)
		{
			waveSamps = AudioUtil.loadAudioFile(filepath);
		}				
	}	
	
	public static void test_fileplayer()
	{
		AudioLoop.HARDWARE = true;		
		AudioLoop.getInstance().register(new FilePlayer("house3.aif"));		
		AudioLoop.getInstance().startAudio();
	}

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void test_rtplayer_with_rt_stream() throws InterruptedException, IOException
	{
		//test_rtplayer_with_rt_stream();
		AudioLoop.ENABLE_STEREO_TEST_MODE = false;
		AudioLoop.getInstance().startAudio();
		
		int NUM_PERFORMERS = 1;
		final int SAMPLE_LENGTH = 180;
		final LinkedList<RtPlayerTest_Performable> tests = new LinkedList<RtPlayerTest_Performable>();
		LinkedList<Thread> threads = new LinkedList<Thread>();
		for(int i=0; i<NUM_PERFORMERS;i++)
		{
			Thread t = new Thread(new Runnable() {
				public void run() {
					tests.add(new RtPlayerTest_Performable(SAMPLE_LENGTH, CircularAudioBuffer.class));					
				}
			});
			t.start(); threads.add(t);
		}
		
		for(Thread t: threads)
			t.join();
		
		System.out.println("---starting tests---");
		//System.out.println("Press any key to listen");
		//System.in.read();
		
		for(RtPlayerTest_Performable test : tests)
			test.startTest();			
	}
	
	public static void test_rtplayer_with_rt_stream_multi() throws InterruptedException, IOException
	{
		//test_rtplayer_with_rt_stream();
		AudioLoop.ENABLE_STEREO_TEST_MODE = true;
		AudioLoop.getInstance().startAudio();
		
		int NUM_PERFORMERS = 2;
		final int SAMPLE_LENGTH = 30;
		final LinkedList<RtPlayerTest_Performable> tests = new LinkedList<RtPlayerTest_Performable>();
		LinkedList<Thread> threads = new LinkedList<Thread>();
		for(int i=0; i<NUM_PERFORMERS;i++)
		{
			Thread t = new Thread(new Runnable() {
				public void run() {
					tests.add(new RtPlayerTest_Performable(SAMPLE_LENGTH, CircularAudioBuffer.class));					
				}
			});
			t.start(); threads.add(t);
		}
		
		for(Thread t: threads)
			t.join();
		
		System.gc();
		Util.sleep(1000);		
		
		System.out.println("---starting tests---");
		//System.out.println("Press any key to listen");
		//System.in.read();
		
		for(RtPlayerTest_Performable test : tests)
			test.startTest();			
	}
	
	public static void test_rtplayer_with_rt_stream_multi2() throws InterruptedException, IOException
	{		
		int NUM_PERFORMERS = 6;
		final int SAMPLE_LENGTH = 900;
		final LinkedList<RtPlayer2Test_Performable> tests = new LinkedList<RtPlayer2Test_Performable>();
		LinkedList<Thread> threads = new LinkedList<Thread>();
		for(int i=0; i<NUM_PERFORMERS;i++)
		{
			Thread t = new Thread(new Runnable() {
				public void run() {
					tests.add(new RtPlayer2Test_Performable(SAMPLE_LENGTH));					
				}
			});
			t.start(); threads.add(t);
		}
		
		for(Thread t: threads)
			t.join();
		
		System.gc();
		Util.sleep(1000);		
		
		System.out.println("---starting tests---");
		//System.out.println("Press any key to listen");
		//System.in.read();
		
		AudioLoop.ENABLE_STEREO_TEST_MODE = true;
		AudioLoop.getInstance().startAudio();
		
		for(RtPlayer2Test_Performable test : tests)
			test.startTest();			
	}
	
	public static void main(String[] args) throws InterruptedException, IOException
	{
		//test_RtPlayerTest_Offline();
		//test_rtplayer_with_rt_stream_multi();
		//test_rtplayer_with_rt_stream();
		//test_rtplayer_with_bounce_stream();
		//test_rtplayer_with_rt_stream_multi();
		test_rtplayer_with_rt_stream_multi2();
		//test_rtplayer_with_rt_stream();  // see change on line 429
		//test_rtplayer_with_bounce_stream();
		//test_fileplayer();
		//test_sine_output();
		//test_opus();
		//testInOutPassThrough__with_hardware_loop();
	}
}
