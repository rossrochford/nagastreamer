package tests;

import java.util.LinkedList;

import messages.RtAudioChunkW;
import serialisers.OpusDecoder;
import serialisers.OpusEncoder;
import workerloops.DecompressionWorker;
import workerloops.DecompressionWorker.DecompressionTask;
import junit.framework.TestCase;

public class DecompressionWorkerTest extends TestCase 
{
	public void test_simple()
	{
		int numChannels = 2;
		int sampleRate = 48000;
		int bitRate = 96000;
		int chunkSize = 960;
		
		float[][] sineSamps = TestUtil.createSineWave(350, numChannels, sampleRate, 48000*4);
		OpusEncoder enc = new OpusEncoder(numChannels, sampleRate, bitRate, chunkSize);
		OpusDecoder dec = new OpusDecoder(numChannels, sampleRate, chunkSize);
		DecompressionWorker.getInstance().start();
		
		LinkedList<RtAudioChunkW> chunks = TestUtil.createChunks(sineSamps, chunkSize, enc);

		for(RtAudioChunkW currChunk : chunks)
			DecompressionWorker.getInstance().queueChunk(currChunk, dec, false); //DecompressionWorker.getInstance().registerTask(new DecompressionWorker.DecompressionTask(currChunk, dec), false);
	
		
	
	
	}
}
