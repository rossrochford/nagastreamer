package tests;

import java.util.LinkedList;

import junit.framework.TestCase;
import messages.RtAudioChunkW;
import workerloops.ClockedRunnable;
import audio.LongAudioBuffer;

public class ClockedRunnableTests extends TestCase
{
	
	public static void test_simple()
	{
		//final long interIterTime = 0;
		
		double expectedInterIterTime = 7.0;
		int totalIterations = 500;
		final LinkedList<Long> iterTimes = new LinkedList<Long>();
		long maxIterTime = 0;
		long minIterTime = 0;
		
		ClockedRunnable r = new ClockedRunnable(expectedInterIterTime, totalIterations){
			
			long lastIterTime = -1;
			
			public void doStuff() {
				long currTime = System.currentTimeMillis();	
				if(lastIterTime != -1)
					iterTimes.add(currTime);
				lastIterTime = currTime;
			}
		};
		
		long startTime = System.currentTimeMillis();
		r.run();
		long endTime = System.currentTimeMillis();
		long dur = (endTime-startTime);
		
		double expectedDur = (totalIterations*expectedInterIterTime);
		
		double actualAvgInterIterTime = 0.0;
		for(int i=1; i<iterTimes.size();i++)
		{
			long interIterTime = iterTimes.get(i)-iterTimes.get(i-1);
			
			actualAvgInterIterTime += interIterTime;
			
			if(interIterTime > maxIterTime)
				maxIterTime = interIterTime;
			if(minIterTime == 0 || interIterTime < minIterTime)
				minIterTime = interIterTime;
		}
		actualAvgInterIterTime = actualAvgInterIterTime/(iterTimes.size()-1);
		
//		System.out.println("avg: "+actualAvgInterIterTime);
//		System.out.println("min: "+minIterTime);
//		System.out.println("max: "+maxIterTime);				
//		System.out.println("total time: "+(endTime-startTime));
//		System.out.println("expected time: "+(totalIterations*expectedInterIterTime));
		
		boolean avgInterIterTimeAsExpected = Math.abs(expectedInterIterTime-actualAvgInterIterTime) < 0.05;
		boolean totalDurationAsExpected = Math.abs(dur-expectedDur) < 2;
		boolean numIterationsAsExpected = iterTimes.size() == totalIterations-1;
		
		assertTrue(avgInterIterTimeAsExpected && totalDurationAsExpected && numIterationsAsExpected);
		
		//assertTrue(Math.abs(expectedInterIterTime-actualAvgInterIterTime) < 0.2);
	}
	
//	public static void main(String[] args)
//	{
//		ClockedRunnableTests.test_simple();
//	}
}
