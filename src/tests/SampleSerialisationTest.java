package tests;

import serialisers.PCM16BitDecoder;
import serialisers.PCM16BitEncoder;
import junit.framework.TestCase;


public class SampleSerialisationTest extends TestCase 
{

	public void test_PCM16Bit_with_1_channel()
	{
		int numChannels = 1;
		int chunkSize = 256;
		
		PCM16BitEncoder encoder = new PCM16BitEncoder();
		
		float[][] samples = new float[numChannels][chunkSize];
		for(int i=0;i<chunkSize;i++)
			samples[0][i] = (float)((Math.random()*2.0)-1.0);
		
		byte[] bytes = encoder.serializeSamples(samples);
		
		PCM16BitDecoder decoder = new PCM16BitDecoder(numChannels, chunkSize);
		
		float[][] deserialisedSamples = decoder.deserialiseSamples(bytes);
		
		boolean sampsRoughlyEqual = true;
		
		for(int i=0; i<chunkSize; i++)
		{
			if(Math.abs(deserialisedSamples[0][i] - samples[0][i]) > 0.00008){
				sampsRoughlyEqual = false;
				break;
			}
		}
		
		assertTrue(sampsRoughlyEqual);
	}
	
	public void test_PCM16Bit_with_2_channe2()
	{
		int numChannels = 2;
		int chunkSize = 256;
		
		PCM16BitEncoder encoder = new PCM16BitEncoder();
		
		float[][] samples = new float[numChannels][chunkSize];
		for(int i=0;i<chunkSize;i++)
		{
			samples[0][i] = (float)((Math.random()*2.0)-1.0);
			samples[1][i] = (float)((Math.random()*2.0)-1.0);
		}
		
		byte[] bytes = encoder.serializeSamples(samples);
		
		PCM16BitDecoder decoder = new PCM16BitDecoder(numChannels, chunkSize);
		
		float[][] deserialisedSamples = decoder.deserialiseSamples(bytes);
		
		boolean sampsRoughlyEqual = true;
		
		for(int i=0; i<chunkSize; i++)
		{
			if(Math.abs(deserialisedSamples[0][i] - samples[0][i]) > 0.00008 || Math.abs(deserialisedSamples[1][i] - samples[1][i]) > 0.00008){
				sampsRoughlyEqual = false;
				break;
			}
		}
		
		assertTrue(sampsRoughlyEqual);
	}
}
