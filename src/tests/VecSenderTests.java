package tests;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

import audio.AudioBuffer;
import audio.LongAudioBuffer;
import audio.AudioLoop;
import audio.LongAudioBuffer;
import audio.MultiVec;
import audio.RtStreamPlayer;
import messages.RtAudioChunkW;
import messages.StartStreamW;
import server.CloudioConnection;
import server.StreamSettings;
import server.VecSender;
import util.Util;
import workerloops.AudioChunkSender;
import junit.framework.TestCase;

public class VecSenderTests  
{
	public static void test_simple()
	{
		int numChannels = 2;
		int sampleRate = 48000;
		int chunkSize = 960;
		int chunkDur = (int) Math.round(Util.sampsToMs(chunkSize, sampleRate));
		StreamSettings ss = new StreamSettings(StartStreamW.BOUNCE_STREAM, 2, 48000, chunkSize, StartStreamW.OPUS, 96000);
		CloudioConnection conn = null; //we'll get away with this provided we don't start the AudioChunkSender (TOOD: consider making a dummy test connection class that simply stores the items its asked to send)
		int vecSize = AudioLoop.VEC_SIZE;
		VecSender vecSender = new VecSender(conn, ss, vecSize);
		
		int sampLen = AudioLoop.SAMPLE_RATE*15;
		
		for(int i=0; i<sampLen; i+=vecSize)
			vecSender.sendVec(new MultiVec(TestUtil.sineSeg(i, 300, numChannels, sampleRate, vecSize)));
		
		long currTime = System.currentTimeMillis();
		LinkedList<RtAudioChunkW> chunks = new LinkedList<RtAudioChunkW>();
		ConcurrentLinkedQueue taskQueue = AudioChunkSender.getInstance().getTaskQueue();
		for(int i=0; !taskQueue.isEmpty(); i++)
		{
			RtAudioChunkW currChunk = ((AudioChunkSender.AudioChunkSendTask)(taskQueue.remove())).chunk;
			currChunk.arrivalTime = currTime + (i*chunkDur);
			chunks.add(currChunk);
		}

		AudioBuffer buf = new LongAudioBuffer(chunks);

		RtStreamPlayer rtPlayer = new RtStreamPlayer(buf, ss, vecSize);
		
		AudioLoop.getInstance().startAudio();
		AudioLoopTest.RtPlayerPerformable p = new AudioLoopTest.RtPlayerPerformable(rtPlayer);
		AudioLoop.getInstance().register(p);
	}
	
	public static void main(String[] args)
	{
		VecSenderTests.test_simple();
	}
}
