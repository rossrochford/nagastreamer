package tests;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;
import messages.EndStreamW;
import messages.RtAudioChunkW;
import messages.SerialisableMessage;
import messages.StartStreamW;
import messages.StatusMessageW;

import org.junit.Test;

import audio.AudioUtil;
import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;
import protobuf.Messages.StatusMessage;
import serialisers.OpusDecoder;
import serialisers.OpusEncoder;
import util.Constants;


public class SerialisationTests2 extends TestCase 
{
	public void test_RtAudioChunkSerialisation_with_audioPayload() 
	{		
		int expPacketNum = 2;
		int expStreamId = 1;
		byte[] payload = new byte[]{1,2};
		List<Integer> expControlVals = new ArrayList<Integer>();
		expControlVals.add(5);
		
		//RtAudioChunkW chunk = new RtAudioChunkW(2, 2, , null);
		
		RtAudioChunkW chunk = new RtAudioChunkW(expPacketNum, payload, expControlVals);
		
		byte[] b = chunk.toBytes();
		
		RtAudioChunkW msg = (RtAudioChunkW)SerialisableMessage.fromBytes(b);
		
		boolean payloadIsAsExpected = msg.audioPayload[0] == payload[0] && msg.audioPayload[1] == payload[1];
		boolean controlValsAsExpected = msg.controlValues.get(0) == expControlVals.get(0);
		
		assertTrue(payloadIsAsExpected && controlValsAsExpected);
	}
	
	public void test_OpusSerialisation()
	{
		double sampleRate = 48000;
		int chunkSize = 960;
		//int numSamps = (int) sampleRate*3; //3 seconds
		int numChannels = 1;
		double testFreq = 350;
		int len = 10000;
		
		float[][] samps = TestUtil.createSineWave(testFreq, numChannels, sampleRate, len);
		float[][] decSamps = new float[numChannels][len];
		
		OpusEncoder enc = new OpusEncoder(numChannels, 48000, 96000, chunkSize);
		OpusDecoder dec = new OpusDecoder(numChannels, 48000, chunkSize);
		
		for(int i=0; i+chunkSize<len; i+=chunkSize)
		{
//			float[][] chunk = new float[numChannels][chunkSize];
//			for(int chan=0; chan<numChannels; chan++)
//				System.arraycopy(samps[chan], i, chunk[chan], 0, chunkSize);
			
			float[][] chunk = AudioUtil.seg(samps, i, chunkSize);
			
			byte[] compBytes = enc.serializeSamples(chunk);
			float[][] decChunkSamps = dec.deserialiseSamples(compBytes);
			
			for(int chan=0; chan<numChannels; chan++)
				System.arraycopy(decChunkSamps[chan], 0, decSamps[chan], i, chunkSize);
		}
		
		boolean pitchEqual = true;
		//boolean isSineShape = true;
		for(int i=5000; i+chunkSize<len; i+=chunkSize) //starting at position 250 just in case the waveform is initially distorted by the codec
		{
			for(int chan=0; chan<numChannels; chan++)
			{	
				float[][] origChunk = AudioUtil.seg(new float[][]{samps[chan]}, i, chunkSize);
				float[][] decChunk = AudioUtil.seg(new float[][]{samps[chan]}, i, chunkSize);
				
				double origPitch = TestUtil.detectPitch(origChunk, sampleRate);
				double decPitch = TestUtil.detectPitch(decChunk, sampleRate);
				
				if(Math.abs(origPitch-decPitch) > 0.1 && origPitch != -1){
					pitchEqual = false;
					break;
				}
				
//				if(!TestUtil.isSineShape(decChunk, testFreq)){
//					isSineShape = false;
//					break;
//				}
			}
		}
		
		assertTrue(pitchEqual); // && isSineShape);
	}
	
	public void test_StartStreamW_message()  //TODO: also test with bitRate value
	{		
		LinkedList<Integer> unitIds = new LinkedList<Integer>();
		unitIds.add(32); unitIds.add(53);
		int unitIndex = 0;
		StartStreamW msg = new StartStreamW(StartStreamW.RT_STREAM, unitIds, unitIndex, "abcde", 2, 48000, 960, StartStreamW.PCM_16Bit);
		
		byte[] bytes = msg.toBytes();
		
		StartStreamW retrievedMsg = StartStreamW.fromBytes(bytes);		
		
		boolean unitIdsEqual = retrievedMsg.unitIds.equals(msg.unitIds);
		boolean unitIdexesEqual = retrievedMsg.unitIndex == msg.unitIndex;
		boolean sessionUuidEqual = retrievedMsg.sessionUuid.equals(msg.sessionUuid);
		boolean numChannelsEqual = retrievedMsg.numChannels == msg.numChannels;
		boolean sampleRateEqual = retrievedMsg.sampleRate == msg.sampleRate;		
		boolean compTypeEqual = retrievedMsg.compressionType == msg.compressionType;
		boolean bitRateEqual = retrievedMsg.bitRate == msg.bitRate; //even though it was omitted we expect it to have a value of -1
		
		assertTrue(sessionUuidEqual && numChannelsEqual && sampleRateEqual && compTypeEqual);
	}
	
//	public void test_StartStreamW_message_with_bitRate()  //TODO: also test with bitRate value
//	{
//		StartStreamW msg = new StartStreamW(1, "abcde", 2, 48000, StartStreamW.OPUS, 128000);
//		
//		new StartStreamW(unitIds, unitIndex, sessionUuid, numChannels, sampleRate, chunkSize, compType, bitRate)
//		
//		byte[] bytes = msg.toBytes();
//		
//		StartStreamW retrievedMsg = StartStreamW.fromBytes(bytes);		
//		
//		boolean compTypeEqual = retrievedMsg.compressionType == msg.compressionType;
//		boolean bitRateEqual = retrievedMsg.bitRate == msg.bitRate;
//		
//		assertTrue(compTypeEqual && bitRateEqual);
//	}
	
	public void test_StreamEndE_message()
	{
		EndStreamW msg = new EndStreamW();
		
		byte[] bytes = msg.toBytes();
		
		EndStreamW retrievedMsg = EndStreamW.fromBytes(bytes);
		
		assertTrue(bytes.length == 1 && bytes[0] == EndStreamW.messageTypeNum);
	}
	
	public void test_status_message()
	{
		StatusMessageW msg = new StatusMessageW(2, "poop");
		
		byte[] bytes = msg.toBytes();
		
		StatusMessageW retrievedMsg = StatusMessageW.fromBytes(bytes);
		
		assertTrue(retrievedMsg.statusNum == msg.statusNum && retrievedMsg.message.equals(msg.message));
	}
}
