package tests;

import junit.framework.TestCase;

public class TestUtil_Test extends TestCase 
{
	public void test_createSineWave()
	{
		double sampleRate = 48000;
		double freq = 14.0; //detectPitch() only seems to work for frequencies up to 1400Hz
		int length = 30000;
		int numChannels = 2;
		
		float[][] sine = TestUtil.createSineWave(freq, numChannels, sampleRate, length);
		
		double pitchL = TestUtil.detectPitch(new float[][]{sine[0]}, sampleRate);
		double pitchR = TestUtil.detectPitch(new float[][]{sine[1]}, sampleRate);
		
		boolean pitchLAlmostEqual = Math.abs(freq-pitchL) < 0.1;
		boolean pitchRAlmostEqual = Math.abs(freq-pitchR) < 0.1;
		boolean lengthCorrect = sine[0].length == length; //we won't check the right channel
		boolean numChannelsCorrect = sine.length == numChannels;
		
		int windowSize = 3;
		double minAngle = 0.0;
		double maxAngle = 0.0;
		for(int i=windowSize; i<sine[0].length; i++)
		{
			double currAngle = (sine[0][i-windowSize] - sine[0][i])/windowSize;
			
			if(currAngle < minAngle)
				minAngle = currAngle;
			if(currAngle > maxAngle)
				maxAngle = currAngle;
		}
		
		System.out.println(minAngle);
		System.out.println(maxAngle);
		
		assertTrue(pitchLAlmostEqual && pitchRAlmostEqual && lengthCorrect && numChannelsCorrect);
	}
	
	public void test_isSineShape()
	{
		double sampleRate = 48000;
		double freq = 400.0;
		int length = 30000;
		int numChannels = 2;
		
		float[][] sine = TestUtil.createSineWave(freq, numChannels, sampleRate, length);
		
		assertTrue(TestUtil.isSineShape(sine));
	}
	
	public void test_isSineShape2()
	{
		double sampleRate = 48000;
		double freq = 4000.0;
		int length = 30000;
		int numChannels = 2;
		
		float[][] sine = TestUtil.createSineWave(freq, numChannels, sampleRate, length);
		
		assertTrue(TestUtil.isSineShape(sine));
	}
}