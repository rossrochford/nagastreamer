package tests;

import util.Util;
import junit.framework.TestCase;

public class UtilTest extends TestCase 
{
	public void test_scale()
	{
		double val = 0.5;
		double srcMin = 0.0;
		double srcMax = 1.0;
		double destMin = 0;
		double destMax = 100;		
		
		double expectedVal = 50.0;
		
		assertEquals(expectedVal, Util.scale(val, srcMin, srcMax, destMin, destMax));
	}
	
	public void test_scale2()
	{
		double val = 5;
		double srcMin = 1;
		double srcMax = 9;
		double destMin = 10;
		double destMax = 110;		
		
		double expectedVal = 60.0;
		
		assertEquals(expectedVal, Util.scale(val, srcMin, srcMax, destMin, destMax));
	}
	
	public void test_sleep()
	{
		int sleepTime = 10;
		
		long startTime = System.currentTimeMillis();
		
		Util.sleep(10);
		
		long endTime = System.currentTimeMillis();
		
		//int dur = (int) (endTime-startTime);
		long dur = endTime-startTime;
		
		assertTrue(Math.abs(dur-sleepTime) < 2);
	}

}
