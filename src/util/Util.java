package util;

import server.CloudioConnection;
import messages.StatusMessageW;
import naga.NIOSocket;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import audio.AudioLoop;

public class Util 
{
	//ArrayList<Long> times = new ArrayList<Long>(410000);
	LinkedList<Long> times = new LinkedList<Long>();
	
	private static LinkedList<Object> preventGCList = new LinkedList<Object>();
	
	public static void keepFromGC(Object o)
	{
		preventGCList.add(o);
		
		if(Math.random() < 0.01)
			System.out.println("preventGCList.size(): "+ preventGCList.size());
	}
	
	public static void sendGenericError(CloudioConnection conn, String errorMessage)
	{
		conn.sendMsg(new StatusMessageW(StatusMessageW.GENERIC_ERROR_MESSAGE, errorMessage));
	}
	
	public static String uuid() //note: not sure if this should be called in the audio callback thread in case it uses System.nanoTime() as a seed which I presume uses I/O?
	{
		return String.valueOf(UUID.randomUUID()); //takes about 300th of a millisecond
	}
	
	public static void sleep(long ms)
	{
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {e.printStackTrace();}
	}
	
	public static double msToNano(double ms)
	{
		return ms*1000000.0;
	}
	
	public static double scale(double val, double srcMin, double srcMax, double destMin, double destMax)
	{
		return (((val - srcMin) / (srcMax-srcMin)) * (destMax-destMin)) + destMin;	//return (((val-srcMin) * (destMax-destMin))/(destMax-destMin)) + destMin;
	}
	
	public static double sampsToMs(double numSamples)
	{
		return sampsToMs(numSamples, AudioLoop.SAMPLE_RATE);
	}
	
	public static double sampsToMs(double numSamples, int sampleRate)
	{
		return 1000.0/(sampleRate/numSamples);
	}
	
	public static double msToSamps(double ms)
	{
		return msToSamps(ms, false, AudioLoop.SAMPLE_RATE);
	}
	
	public static double msToSamps(double ms, boolean round)
	{
		return msToSamps(ms, round, AudioLoop.SAMPLE_RATE);
	}
	
	public static double msToSamps(double ms, boolean round, int sampleRate)
	{
		if(round)
			return Math.round((ms/1000.0)*sampleRate);
		else
			return (ms/1000.0)*sampleRate;
	}
	
	public void doStuff()
	{
		long startTime = System.currentTimeMillis();
		for(int i=0; i<2000000; i++){
			times.add(System.currentTimeMillis());
			if(i > 100000)
				times.remove(0);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("dur:"+(endTime-startTime)/1000.0);
		
		long timeSum = 0;
		long max = 0;
		int maxPos = 0;
		for(int i=1; i<times.size(); i++){
			long timestampDur = times.get(i) - times.get(i-1);
			timeSum += timestampDur;
			if(timestampDur > max){
				max = timestampDur;
				maxPos = i;
			}
		}
		double avg = timeSum/(times.size() + 0.0);
		
		System.out.println("avg:"+ avg);
		System.out.println("max:"+ max);
		System.out.println("maxPos:"+ maxPos);
		
//		long startTime = System.currentTimeMillis();
//		long lastTimestamp = -1;
//		for(int i=0; i<2000000; i++)
//		{
//			if(i % 5 == 0){
//				lastTimestamp = System.currentTimeMillis();
//				times.add(lastTimestamp);				
//			}
//			else
//				times.add(lastTimestamp);
//		}
//		
//		
//		long endTime = System.currentTimeMillis();
//		
//		System.out.println((endTime-startTime)/1000.0);
	}
	
	public static double averageList(List l)
	{
		double avg = 0.0;
		for(int i=0; i<l.size(); i++)
		{
			double val = (double) l.get(i); //hopefully this will work for floats too
			avg += val;
		}
		avg = avg/l.size();
		
		return avg;
	}
	
	public static void printIfDebug(String s)
	{
		if(Constants.DEBUG)
			System.out.println(s);
	}
	
	public static void main(String[] args)
	{
		//new Util().doStuff();
		System.out.println(Util.scale(0.5, 0.0, 1.0, 0, 100));
	}
}
