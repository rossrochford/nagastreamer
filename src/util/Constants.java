package util;

public class Constants 
{
	//compression types are located in StartStreamW
	
	public static final int listenPort = 6767;
	
	public static String serverUserUuid; //TODO: needs to be set
	
	public static boolean DISABLE_GC = false;
	
	public static boolean AVOID_MEMORY_ALLOCATION = true;
	
	public static boolean DEBUG = true;	
}
