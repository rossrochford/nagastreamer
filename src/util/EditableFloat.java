package util;

public class EditableFloat 
{
	public float val;
	
	public EditableFloat(float val)
	{
		this.val = val;
	}
	
	public EditableFloat(double val)
	{
		this.val = (float) val;
	}
	
	public void set(float val)
	{
		this.val = val;
	}
}