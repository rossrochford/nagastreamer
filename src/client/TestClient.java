package client;

import java.net.InetSocketAddress;
import java.nio.LongBuffer;
import java.util.LinkedList;

import audio.AudioBuffer;
import audio.AudioLoop;
import audio.AudioUtil;
import audio.CircularAudioBuffer;
import audio.LongAudioBuffer;
import audio.MultiVec;
import audio.Performable;
import audio.RtStreamPlayer;
import serialisers.SampleSerialiser;
import server.CloudioConnection;
import server.Connection;
import server.RtStream;
import server.StreamSettings;
import server.VecSender;
import tests.TestUtil;
import util.Constants;
import util.Util;
import workerloops.ClockedRunnable;
import messages.EndStreamW;
import messages.RtAudioChunkW;
import messages.StartStreamW;
import naga.NIOSocket;
import naga.SocketObserverAdapter;

public class TestClient
{
	public final CloudioConnection connection = new CloudioConnection();
	public AudioBuffer receiveBuffer;
	
	public void connect()
	{
		ClientConnectionListener listener = new ClientConnectionListener(this); //TODO: make this pass packets to an RtStream object and register this with the audio loop
		connection.connect(new InetSocketAddress("127.0.0.1", Constants.listenPort), "TEST_CLIENT", 1, listener);
	}
	
	public void receiveChunk(RtAudioChunkW chunk)
	{
		receiveBuffer.addChunk(chunk);  // we'll assume its already been created
	}
	
	public void startStream(final LinkedList<RtAudioChunkW> chunks, StreamSettings ss)
	{
		if(!connection.isConnected()){
			System.out.println("TestClient: cannot start stream because we're not connected to the server");
			return;
		}			
		
		receiveBuffer = new CircularAudioBuffer();

		LinkedList<Integer> units = new LinkedList<Integer>(); units.add(new Integer(0));
		connection.sendMsg(new StartStreamW(StartStreamW.RT_STREAM, units, 0, "--TEST-SESSION--", ss));

		final RtStreamPlayer rtStreamPlayer = new RtStreamPlayer(receiveBuffer, ss, AudioLoop.VEC_SIZE);
		final VecSender vecSender = new VecSender(connection, ss, AudioLoop.VEC_SIZE);
		
		Thread t = new Thread(new ClockedRunnable(Util.sampsToMs(ss.chunkSize)){ //disadvantage of this is that its difficult to stop the stream
			int chunkNum = 0;
			public void doStuff() {
				if(chunkNum < chunks.size()){
					connection.sendMsg(chunks.get(chunkNum));
					chunkNum++;
				}
				else{
					System.out.println("TestClient: ending stream");
					connection.sendMsg(new EndStreamW()); connection.disconnect();
				}					
			}
		}); t.start(); 
		
		final Performable p = new Performable() {
			int sampIndex = 0;
			public void perform(float[][] in, final float[][] out) {
				rtStreamPlayer.playVec(new MultiVec(out));
				
				vecSender.sendVec(new MultiVec(AudioUtil.seg(chunks.get(sampIndex/AudioLoop.VEC_SIZE).samples, sampIndex%AudioLoop.VEC_SIZE, AudioLoop.VEC_SIZE)));
				
				sampIndex += AudioLoop.VEC_SIZE;
			}
		};
		
		AudioLoop.getInstance().register(p);
	}
	
	public static void main(String[] args)
	{
		AudioLoop.HARDWARE = true;  //should be false for the server
		int chunkSize = 960;
		int numChannels = 2;
		int sampleRate = 48000;
		
		final TestClient client = new TestClient();
		client.connect();
		Util.sleep(1000);
		
		float[][] samps = TestUtil.createSineWave(300, 2, 48000, 48000*9);	
		StreamSettings ss = new StreamSettings(StartStreamW.RT_STREAM, numChannels, sampleRate, chunkSize, StartStreamW.OPUS, 128000);
		final LinkedList<RtAudioChunkW> chunks = TestUtil.createChunks(samps, chunkSize, SampleSerialiser.getEncoder(ss));
	
		client.startStream(chunks, ss);		
	}
}
