package serialisers;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

import org.jitsi.impl.neomedia.codec.audio.opus.Opus;

import util.Constants;
import audio.AudioUtil;

import com.petersalomonsen.jjack.javasound.ByteIntConverter;

public class OpusEncoder 
{
	int numChannels;
	int chunkSize;
	long encoderId;
	AudioFormat codecInputFormat;
	AudioFormat floatAudioFormat; //its best we create this up front
	
	byte[] reusableCompressedBytesHolder;
	byte[] reusableLittleEndianBytesHolder;
	
	public OpusEncoder(int numChannels, int sampleRate, int bitRate, int chunkSize)
	{
		this.numChannels = numChannels;
		
		this.encoderId = Opus.encoder_create(sampleRate, numChannels);
		Opus.encoder_set_bitrate(encoderId, bitRate);
		Opus.encoder_set_bandwidth(encoderId, Opus.BANDWIDTH_FULLBAND);
		
		this.chunkSize = chunkSize;
		this.codecInputFormat = new AudioFormat(sampleRate, 16, numChannels, true, false); //signed=true, isBigEndian=false
		//this.floatAudioFormat = new AudioFormat(AudioFormat.Encoding.F, sampleRate, sampleSizeInBits, channels, frameSize, frameRate, bigEndian)
		
		reusableCompressedBytesHolder = new byte[Opus.MAX_PACKET]; //to save memory allocataionts
		reusableLittleEndianBytesHolder = new byte[chunkSize*2*numChannels];		
	}
	
	/*public ByteArrayInputStream serializeSamples(AudioInputStream stream)
	{
		stream = new AudioInputStream(stream, codecInputFormat, stream.getFrameLength());		
		
		byte[] littleEndianSampleBytes = null;
		try {
			littleEndianSampleBytes = new byte[stream.available()];
			stream.read(littleEndianSampleBytes);
		} catch (IOException e) {e.printStackTrace(); return null;}
		
		byte[] compressedSampleBytes = new byte[Opus.MAX_PACKET];
		
		int resLen = Opus.encode(encoderId, littleEndianSampleBytes, 0, chunkSize, compressedSampleBytes, 0, compressedSampleBytes.length);
		
		return new ByteArrayInputStream(compressedSampleBytes, 0, resLen);
	}*/
	
	public byte[] serializeSamples(float[][] samples, int resultByteOffset) 
	{
		byte[] littleEndianSampleBytes = Constants.AVOID_MEMORY_ALLOCATION ? AudioUtil.floatToLittleEndian16Bit(samples, reusableLittleEndianBytesHolder) : AudioUtil.floatToLittleEndian16Bit(samples);
		
		byte[] compressedSampleBytes = Constants.AVOID_MEMORY_ALLOCATION ? reusableCompressedBytesHolder : new byte[Opus.MAX_PACKET];	

		int resLen = Opus.encode(encoderId, littleEndianSampleBytes, 0, chunkSize, compressedSampleBytes, 0, compressedSampleBytes.length);

		byte[] compressedBytes = new byte[resLen+resultByteOffset]; //could keep a hash map of these, hashed by length, or even have a centralised object pool (though perhaps this would require synchronisation? which would create a bottleneck)
        System.arraycopy(compressedSampleBytes, 0, compressedBytes, resultByteOffset, resLen);		
		return compressedBytes;
	}
	
	/*public ByteArrayInputStream serializeSamples_stream(float[][] samples) //because it might be more efficient to return a stream for sending audio 
	{
		byte[] littleEndianSampleBytes = AudioUtil.floatToLittleEndian16Bit(samples);		
		byte[] compressedSampleBytes = new byte[Opus.MAX_PACKET];	
		int resLen = Opus.encode(encoderId, littleEndianSampleBytes, 0, chunkSize, compressedSampleBytes, 0, compressedSampleBytes.length);
		
		return new ByteArrayInputStream(compressedSampleBytes, 0, resLen);
	}*/
	
	protected void finalize() throws Throwable
	{
		Opus.encoder_destroy(encoderId);		
		super.finalize();
	}
	
//	public byte[] serializeSamples_old(float[][] samples) 
//	{
//		byte[] littleEndianSampleBytes = new byte[chunkSize*2*numChannels]; //opus wants little endian so we convert the floats to this first
//        
//		if(numChannels == 1)
//		{
//			for(int i=0; i<chunkSize; i++)
//	        {
//	            short shortVal = (short)(samples[0][i]*32767.0);
//	            littleEndianSampleBytes[(i*2)] = (byte)shortVal;
//	            littleEndianSampleBytes[(i*2)+1] = (byte)(shortVal >> 8);
//	        }
//		}
//		else if(numChannels == 2)
//		{
//			for(int i=0; i<chunkSize; i++)
//	        {
//	            short shortValL = (short)(samples[0][i]*32767.0);
//	            littleEndianSampleBytes[(i*4)] = (byte)shortValL;
//	            littleEndianSampleBytes[(i*4)+1] = (byte)(shortValL >> 8);
//	            
//	            short shortValR = (short)(samples[1][i]*32767.0);
//	            littleEndianSampleBytes[(i*4)+2] = (byte)shortValR;
//	            littleEndianSampleBytes[(i*4)+3] = (byte)(shortValR >> 8);
//	        }
//		}
//		else
//			System.out.println("invalid number of channel!");		
//		
//		byte[] compressedSampleBytes = new byte[Opus.MAX_PACKET];
//		
//		int resLen = Opus.encode(encoderId, littleEndianSampleBytes, 0, chunkSize, compressedSampleBytes, 0, compressedSampleBytes.length);
//		
//		byte[] compressedBytes = new byte[resLen];
//      System.arraycopy(compressedSampleBytes, 0, compressedBytes, 0, resLen);
//		
//		return compressedBytes;
//	}
}
