package serialisers;


//should probably be called 'BigEndianPCMdecorder' (or is it little-endian?)
public class PCM16BitDecoder extends SampleDeserialiser
{
	
	int numChannels;
	int chunkSize;
	
      public PCM16BitDecoder(int numChannels, int chunkSize)
      {
    	  this.numChannels = numChannels;
    	  this.chunkSize = chunkSize;           
      }
      
      public float[][] deserialiseSamples(byte[] payloadBytes) 
      {                
          float[][] samples = new float[numChannels][chunkSize];
          
          int pos = 0;
          
          if(numChannels == 1)
          {
        	  for(int i=0; i<chunkSize; i++)
        	  {
        		  short tmpShrt = (short) ( ((((short)payloadBytes[i*2])<<8)) | (payloadBytes[(i*2)+1] & 0xFF));
        		  samples[0][i] = (float) ((tmpShrt)/32767.0);
        	  }
          }
          else if(numChannels == 2)
          {
        	  for(int i=0; i<chunkSize; i++)
        	  {
        		  short tmpShrtL = (short) ( ((((short)payloadBytes[i*4])<<8)) | (payloadBytes[(i*4)+1] & 0xFF));
        		  short tmpShrtR = (short) ( ((((short)payloadBytes[(i*4)+2])<<8)) | (payloadBytes[(i*4)+3] & 0xFF));

        		  samples[0][i] = (float) ((tmpShrtL)/32767.0);
        		  samples[1][i] = (float) ((tmpShrtR)/32767.0);
        	  }
          }
          else
        	  System.out.println("warning: unsupported num channels");     

          return samples;
      }
}