package serialisers;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

import org.jitsi.impl.neomedia.codec.audio.opus.Opus;

import server.Connection;
import util.Constants;
import audio.AudioLoop;

public class OpusDecoder extends SampleDeserialiser 
{
	int numChannels;
	int chunkSize;
	long decoderId;
	int decodeFEC = 0;
	AudioFormat codecOutputFormat; 
	
	byte[] reusableLittleEndianBytesHolder;
	float[][] reusableFloatsHolder;
	
	public OpusDecoder(int numChannels, int sampleRate, int chunkSize) //I don't think we really need to know the chunkSize ahead of time actually, its just convenient for knowing how much memory to allocate.
	{
		this.numChannels = numChannels;
		this.chunkSize = chunkSize;
		codecOutputFormat = new AudioFormat(sampleRate, 16, numChannels, true, false); //(signed=true and isBigEndian=false)
		
		reusableFloatsHolder = new float[numChannels][chunkSize];
		this.reusableLittleEndianBytesHolder = new byte[chunkSize*2*numChannels];
		
		decoderId = Opus.decoder_create(sampleRate, numChannels);
	}
	
	/*public AudioInputStream deserialiseSamples(ByteArrayInputStream stream)
	{
		byte[] decompressedFrameBytes = new byte[chunkSize*2*numChannels]; // 2 bytes per sample
		
		byte[] chunkBytes = null;
		try{
			chunkBytes = new byte[stream.available()];
			stream.read(chunkBytes);
		} catch (IOException e) {e.printStackTrace(); return null;}
		
		int numSampsDecompressed = Opus.decode(decoderId, chunkBytes, 0, chunkBytes.length, decompressedFrameBytes, 0, decompressedFrameBytes.length, decodeFEC);
		
		AudioInputStream res = new AudioInputStream(new ByteArrayInputStream(decompressedFrameBytes), codecOutputFormat, codecOutputFormat.getFrameSize());
		res = new AudioInputStream(res, AudioLoop.HARDWARE_AUDIO_FORMAT, res.getFrameLength());
		
		return res;
	}*/
	
	public float[][] deserialiseSamples(byte[] payload)
	{
		return deserialiseSamples(payload, 0);
	}
	
	public float[][] deserialiseSamples(byte[] payload, int offset) 
	{
        byte[] decompressedFrameBytes = Constants.AVOID_MEMORY_ALLOCATION ? reusableLittleEndianBytesHolder : new byte[chunkSize*2*numChannels]; //new byte[chunkSize*2*numChannels]; 
        
        //int numSampsDecompressed = Opus.decode(decoderId, payloadBytes, 0, payloadBytes.length, decompressedFrameBytes, decompressedFrameBytes.length, decodeFEC);
        int numSampsDecompressed = Opus.decode(decoderId, payload, offset, payload.length, decompressedFrameBytes, 0, decompressedFrameBytes.length, decodeFEC);
       
        //convert little endian 16 bit numbers back into floats
        //float[][] sampleFloats = Constants.AVOID_MEMORY_ALLOCATION ? reusableFloatsHolder : new float[numChannels][chunkSize];
        float[][] sampleFloats = new float[numChannels][chunkSize];
        
        if(numChannels == 1)
        {
        	for(int k=0; k<chunkSize; k++){
        		short shortVal = (short) ( ((((short)decompressedFrameBytes[(k*2)+1])<<8)) | (decompressedFrameBytes[(k*2)] & 0xFF) );               
                sampleFloats[0][k] = (float)(shortVal/32767.0);
            }
        }
        else if(numChannels == 2)
        {
        	for(int k=0; k<chunkSize; k++){
        		short shortValL = (short) ( ((((short)decompressedFrameBytes[(k*4)+1])<<8)) | (decompressedFrameBytes[(k*4)] & 0xFF) );               
                sampleFloats[0][k] = (float)(shortValL/32767.0);                
                short shortValR = (short) ( ((((short)decompressedFrameBytes[(k*4)+3])<<8)) | (decompressedFrameBytes[(k*4)+2] & 0xFF) );               
                sampleFloats[1][k] = (float)(shortValR/32767.0);
        	}
        }
        else
        	System.out.println("invalid num channels");
        
		return sampleFloats;
	}
	
	public void deserialiseSamples(byte[] payload, int offset, float[][] floatBuf, int floatBufOffset) 
	{
        byte[] decompressedFrameBytes = Constants.AVOID_MEMORY_ALLOCATION ? reusableLittleEndianBytesHolder : new byte[chunkSize*2*numChannels]; //new byte[chunkSize*2*numChannels]; 
        
        int numSampsDecompressed = Opus.decode(decoderId, payload, offset, payload.length, decompressedFrameBytes, 0, decompressedFrameBytes.length, decodeFEC);
       
        if(numSampsDecompressed != chunkSize)
        	System.out.println("warning: numSampsDecompressed != chunkSize");
        
        if(numChannels == 1)
        {
        	for(int k=0; k<chunkSize; k++){
        		short shortVal = (short) ( ((((short)decompressedFrameBytes[(k*2)+1])<<8)) | (decompressedFrameBytes[(k*2)] & 0xFF) );               
                floatBuf[0][k+floatBufOffset] = (float)(shortVal/32767.0);
            }
        }
        else if(numChannels == 2)
        {
        	for(int k=0; k<chunkSize; k++){
        		short shortValL = (short) ( ((((short)decompressedFrameBytes[(k*4)+1])<<8)) | (decompressedFrameBytes[(k*4)] & 0xFF) );               
        		floatBuf[0][k+floatBufOffset] = (float)(shortValL/32767.0);
                
                short shortValR = (short) ( ((((short)decompressedFrameBytes[(k*4)+3])<<8)) | (decompressedFrameBytes[(k*4)+2] & 0xFF) );               
                floatBuf[1][k+floatBufOffset] = (float)(shortValR/32767.0);
        	}
        }
        else
        	System.out.println("invalid num channels");
	}
	
	protected void finalize() throws Throwable
	{
		Opus.decoder_destroy(decoderId);
	}
}
