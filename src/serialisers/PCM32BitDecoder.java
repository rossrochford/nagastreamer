package serialisers;

import messages.RtAudioChunkW;

public class PCM32BitDecoder 
{
	int numChannels;
	int chunkSize;
	
	public PCM32BitDecoder(int numChannels, int chunkSize) //could generalise this class so that 8 and 32 bit implementations are the same
	{
		this.numChannels = numChannels;
		this.chunkSize = chunkSize;
	}
	
	public void deserialiseSamples(byte[] bytes) //should we be passing the chunk itself so that the chunk number can be tracked?
	{
		float[][] samples = new float[numChannels][chunkSize];
		
		if(numChannels == 1)
		{	
			
			
			for(int i=0; i < chunkSize; i++)
			{
				samples[0][i] = (float)(bytes[i/4] * 32732.0);
				samples[0][i] = (float)(bytes[(i/4)+1] * 32732.0);
			}
		}
		
		//do stuff
		
		//chunk.samples = null; //result
	}
	
	
	
	
	
	public byte[] serialiseSamples(float[][] samples)
	{
		int numSamples = samples[0].length;
		int numChannels = samples.length;
		
		byte[] bytes = new byte[numChannels * 4 * numSamples];
		
		for(int i=0; i<numSamples; i++)
		{
			if(numChannels == 1)
			{
				bytes[i] = (byte)((long)(samples[0][i] * 32732) >> (3 * 8));
				bytes[i+1] = (byte)((long)(samples[0][i] * 32732) >> (2 * 8));
				bytes[i+2] = (byte)((long)(samples[0][i] * 32732) >> (1 * 8));
				bytes[i+3] = (byte)((long)samples[0][i] * 32732);
			}
			else if(numChannels == 2)
			{
				
			}
			else
				System.out.println();
		}			
		
		return bytes;
	}
}
