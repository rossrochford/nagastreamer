package serialisers;

import java.io.ByteArrayInputStream;

import server.StreamSettings;
import messages.RtAudioChunkW;
import messages.StartStreamW;

public abstract class SampleDeserialiser 
{
	
	public abstract float[][] deserialiseSamples(byte[] payload);
	
	public abstract float[][] deserialiseSamples(byte[] payload, int offset);
	
	public static SampleDeserialiser getDecoder(StreamSettings ss)
	{
		return getDecoder(ss.compType, ss.numChannels, ss.sampleRate, ss.chunkSize);
	}
	
	public static SampleDeserialiser getDecoder(int compType, int numChannels, int sampleRate, int chunkSize)
	{
		if(compType == StartStreamW.PCM_16Bit)
			return new PCM16BitDecoder(numChannels, chunkSize);
		else if(compType == StartStreamW.OPUS)
			return new OpusDecoder(numChannels, sampleRate, chunkSize);
			
		
		return null;
	}
}
