package serialisers;

import server.StreamSettings;
import messages.StartStreamW;


public abstract class SampleSerialiser 
{
	public int chunkSize = -1; //-1 because some samples serialisers (e.g. PCM) won't any constant chunk size
	
	public abstract byte[] serializeSamples(float[][] samples);
	
	public static SampleSerialiser getEncoder(StreamSettings ss)
	{
		return getEncoder(ss.compType, ss.numChannels, ss.chunkSize);
	}
	
	public static SampleSerialiser getEncoder(int compType, int numChannels, int chunkSize) // not sure what arguments we need
	{
		if(compType == StartStreamW.PCM_16Bit)
			return new PCM16BitEncoder();
		
		return null;
	}
}