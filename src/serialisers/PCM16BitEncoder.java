package serialisers;


public class PCM16BitEncoder extends SampleSerialiser
{       
		static int bytesPerSample = 2;
	
        public PCM16BitEncoder()
        {
        	System.out.println("PCMEncoder created");
        }
        
        public byte[] serializeSamples(float[][] sampleFloats) 
        {
        	int numChannels = sampleFloats.length;
        	int chunkSize = sampleFloats[0].length;
        	
        	byte[] payloadBytes = new byte[chunkSize*bytesPerSample*numChannels];
                
            if(numChannels == 1)
            {
            	for(int i = 0; i < chunkSize; i++)
                {
                    float clippedValue = Math.max(-1.0f, Math.min(1.0f, sampleFloats[0][i]));                    
                    short shortVal = (short)(clippedValue*32767.0); //shorts have range: -32768 to 32767. There must be a better way of doing this mapping!           
                    
                    payloadBytes[(i*2)] = (byte)(shortVal >> 8);
                    payloadBytes[(i*2) + 1] = (byte)shortVal;                                        
                }                            
            }
            else if(numChannels == 2)
            {
            	for(int i = 0; i < chunkSize; i++)
                {
                    float clippedValueL = Math.max(-1.0f, Math.min(1.0f, sampleFloats[0][i]));                    
                    short shortValL = (short)(clippedValueL*32767.0); //shorts have range: -32768 to 32767. There must be a better way of doing this mapping!           
                    float clippedValueR = Math.max(-1.0f, Math.min(1.0f, sampleFloats[1][i]));                    
                    short shortValR = (short)(clippedValueR*32767.0);
                    
                    payloadBytes[(i*4)] = (byte)(shortValL >> 8);
                    payloadBytes[(i*4) + 1] = (byte)shortValL;                         
                    payloadBytes[(i*4) + 2] = (byte)(shortValR >> 8);
                    payloadBytes[(i*4) + 3] = (byte)shortValR;
                }
            }
            else
            	System.out.println("warning: unsupported num channels");
                
            return payloadBytes;
        }        
}