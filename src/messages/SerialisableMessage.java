package messages;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.google.protobuf.ByteString;

import protobuf.Messages;
import protobuf.Messages.RtAudioChunk.Builder;


public abstract class SerialisableMessage 
{
//	static HashMap<Integer, Class> classMap = new HashMap<Integer, Class>(){
//		{
//			put(1, Messages.RtAudioChunk.class);
//		}
//	};
	
	static Class[] fromBytesParamTypes = new Class[]{byte[].class, Integer.class};
	
	public static HashMap<Integer, Class> classMap = new HashMap<Integer, Class>(){
		{
			put(0, RegisterConnectionW.class);
			put(1, RtAudioChunkW.class);
			put(2, StartStreamW.class);
			put(3, EndStreamW.class);
			put(4, StatusMessageW.class);
		}
	};
	
	static int messageTypeNum = -1;
	
	
	public abstract byte[] toBytes();
	
	
	public static Class getMessageClass(int messageTypeNum)
	{
		return classMap.get(messageTypeNum);
	}
	
	public static int getMessageNum()
	{
		return messageTypeNum;
	}
	
//	public static SerialisableMessage fromBytes(int messageTypeNumBeingDecoded, byte[] bytes)
//	{
//		//based on message type of this class, do stuff
//		
//		if(messageTypeNumBeingDecoded == RtAudioChunkW.messageTypeNum)
//		{
//			
//		}
//		
//		if(messageType == RtAudioChunkW.class)
//		{
//			//blah
//		}
//		
//		
//		return null;
//	}
	
	//without messageTypeNum prefix
	public static byte[] _createBytes(com.google.protobuf.GeneratedMessage.Builder builder)
	{		
		return builder.build().toByteArray();
	}
	
	public static byte[] _createBytes(int messageTypeNum, com.google.protobuf.GeneratedMessage.Builder builder)
	{
		if(messageTypeNum == -1)
			System.out.println("warning messageTypeNum == -1 which suggests that a subclass of Serialisable message hasn't got its messageTypeNum set yet");
		
		ByteString prefix = ByteString.copyFrom(new byte[]{(byte)messageTypeNum});
		
		//byte[] p = prefix.toByteArray();

		ByteString body = builder.build().toByteString();
		
		return prefix.concat(body).toByteArray();
	}
	
	public static SerialisableMessage fromBytes(byte[] bytes) //TODO: should we return null if deserialisation fails?
	{		
		int num = bytes[0];
		
		if(!classMap.containsKey(num))
			return null;
		
		Class c = classMap.get(num);
		
		
		SerialisableMessage result = null;
		try
		{
			Method m = c.getDeclaredMethod("fromBytes", fromBytesParamTypes); //TODO: consider doing this introspection up front
			result = (SerialisableMessage)m.invoke(c, bytes, 1);
		}
		catch(Exception e){
			System.out.println("error introspecting");
		}
		
		return result;		
	}
	
//	public static SerialisableMessage fromBytes(byte[] bytes)
//	{
//		int incommingMessageTypeNum = bytes[0];
//		Class messageClass = classMap.get(incommingMessageTypeNum);
//		((com.google.protobuf.GeneratedMessage)messageClass).
//		
//		return null;
//	}
}
