package messages;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

import protobuf.Messages;
import serialisers.SampleDeserialiser;
import serialisers.SampleSerialiser;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;


public class RtAudioChunkW extends SerialisableMessage implements java.io.Serializable
{
	public static int messageTypeNum = 1;
	
	public int packetNum;
	public byte[] audioPayload = null;
	public int audioPayloadOffset = 0;
	public long arrivalTime;
	public char state = 'C';  //states:  C, Q, D

	
	public RtAudioChunkW(int packetNum, byte[] audioPayload, int audioPayloadOffset)
	{
		this.packetNum = packetNum;
		this.audioPayload = audioPayload;
		this.audioPayloadOffset = audioPayloadOffset;
	}
	
	public byte[] toBytes()// (boolean withPrefix)
	{		
		//byte[] packetNumBytes = new byte[] {(byte)(packetNum >>> 24), (byte)(packetNum >>> 16), (byte)(packetNum >>> 8), (byte)packetNum};
		//byte[] packetNumBytes = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(packetNum).array();
		
		if(audioPayloadOffset == 5){
			audioPayload[0] = (byte)messageTypeNum;
			audioPayload[1] = (byte)(packetNum >>> 24);
			audioPayload[2] = (byte)(packetNum >>> 16);
			audioPayload[3] = (byte)(packetNum >>> 8);
			audioPayload[4] = (byte)(packetNum);
			return audioPayload;
		}
		
		System.out.println("warning: RtAudioChunkW._toBytes() is more memory efficient when audioPayloadOffset == 5");
		
		ByteBuffer buf = ByteBuffer.allocate(4);
		buf.put((byte)messageTypeNum);  //messageType
		buf.order(ByteOrder.BIG_ENDIAN).putInt(packetNum);  //audio packet num
		buf.put(audioPayload, audioPayloadOffset, audioPayload.length-audioPayloadOffset);  //audio payload
		
		return buf.array();		
	}
	
	public static RtAudioChunkW fromBytes(byte[] bytes, int offset)
	{	
		ByteBuffer buf = ByteBuffer.wrap(bytes, offset, bytes.length-offset);
		
		int packetNum = buf.order(ByteOrder.BIG_ENDIAN).getInt();
		
		byte[] audioPayload = bytes;
		int audioPayloadOffset = offset + 4;  //should be 5 bytes. messageTypeNum + paketNum = 5 bytes
		
		return new RtAudioChunkW(packetNum, audioPayload, audioPayloadOffset);
	}
	
	public void timestamp()
	{
		arrivalTime = System.currentTimeMillis();
	}
}
