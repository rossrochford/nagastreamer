package messages;

import java.util.LinkedList;
import java.util.List;

import naga.NIOSocket;
import protobuf.Messages;
import server.StreamSettings;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

public class StartStreamW extends SerialisableMessage implements Cloneable
{	
	static int messageTypeNum = 2; //TODO: should be getting this from a Constants class
	
	public static final int OPUS = 1;
	public static final int PCM_32Bit = 2;
	public static final int PCM_16Bit = 3;
	public static final int PCM_8Bit = 4;
	
	public static final int RT_STREAM = 1;
	public static final int BOUNCE_STREAM = 2;
	
	public int streamType; //TODO: ADD THIS TO POTOBUF MESSAGE DEFINITION!
	//public int streamId = 0;  // consider changing the name of this to something like "audioTransferChannelNum", because we want to use this for bounce uploads also
	public String sessionUuid;
	public List<Integer> unitIds;
	public int unitIndex; //TODO: ADD THIS TO POTOBUF MESSAGE DEFINITION!
	public int numChannels;
	public int sampleRate;
	public int compressionType;
	public int bitRate;
	public int chunkSize;
	
	public StartStreamW(int streamType, List<Integer> unitIds, int unitIndex, String sessionUuid, StreamSettings ss)
	{
		this(streamType, unitIds, unitIndex, sessionUuid, ss.numChannels, ss.sampleRate, ss.chunkSize, ss.compType, ss.bitRate);
	}
	
	public StartStreamW(int streamType, List<Integer> unitIds, int unitIndex, String sessionUuid, int numChannels, int sampleRate, int chunkSize, int compType)
	{
		this(streamType, unitIds, unitIndex, sessionUuid, numChannels, sampleRate, chunkSize, compType, -1);
	}
	
	public StartStreamW(int streamType, List<Integer> unitIds, int unitIndex, String sessionUuid, int numChannels, int sampleRate, int chunkSize, int compType, int bitRate)
	{
		this.streamType = streamType;
		this.sessionUuid = sessionUuid;
		this.unitIds = unitIds;
		this.numChannels = numChannels;
		this.sampleRate = sampleRate;
		this.chunkSize = chunkSize;
		this.compressionType = compType;
		this.bitRate  = bitRate;
	}
	
	public byte[] toBytes() 
	{
		protobuf.Messages.StartStream.Builder builder = protobuf.Messages.StartStream.newBuilder();
		
//		if(streamId != 0)
//			builder.setStreamId(streamId);
		builder.setStreamType(streamType);
		builder.setSessionUuid(sessionUuid);
		builder.setNumChannels(numChannels);
		builder.setSampleRate(sampleRate);
		builder.setChunkSize(chunkSize);
		builder.addAllUnitIds(unitIds);
		builder.setCompressionType(compressionType);
		if(bitRate != -1)
			builder.setBitRate(bitRate);
		
		return _createBytes(messageTypeNum, builder);
	}
	
	public static StartStreamW fromBytes(byte[] bytes)
	{
		Messages.StartStream msg = null;
		
		try {
			msg = Messages.StartStream.parseFrom(ByteString.copyFrom(bytes, 1, bytes.length-1));
		} catch (InvalidProtocolBufferException e) {return null;}
		
		//int streamId = (msg.hasStreamId()) ? msg.getStreamId() : 0;
		int bitRate = (msg.hasBitRate())? msg.getBitRate() : -1; 
		
		if(msg.getUnitIdsCount() == 0){
			System.out.println("warning: StartStream message has no unitIds");
			return null;
		}
		
		return new StartStreamW(msg.getStreamType(), msg.getUnitIdsList(), msg.getUnitIndex(), msg.getSessionUuid(), msg.getNumChannels(), msg.getSampleRate(), msg.getChunkSize(), msg.getCompressionType(), bitRate);
		
		//return new StartStreamW(msg.getUnitIdsList(), msg.getSessionUuid(), msg.getNumChannels(), msg.getSampleRate(), msg.getChunkSize(), msg.getCompressionType(), bitRate);
	}	
}
