package messages;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

public class EndStreamW extends SerialisableMessage
{
	public static int messageTypeNum = 3;
	//public int streamId = 0;

	public EndStreamW()
	{
		super();
	}
	
	public byte[] toBytes()
	{
		//protobuf.Messages.EndStream.Builder builder = protobuf.Messages.EndStream.newBuilder();
		//if(streamId != 0)
			//builder.setStreamId(streamId);
		
		//return _createBytes(messageTypeNum, builder);
		
		return new byte[]{(byte)messageTypeNum};
	}
	
	public static EndStreamW fromBytes(byte[] bytes)
	{
//		protobuf.Messages.EndStream msg = null;
//		
//		try {
//			msg = protobuf.Messages.EndStream.parseFrom(ByteString.copyFrom(bytes, 1, bytes.length-1));
//		} catch (InvalidProtocolBufferException e) {return null;}
//		
//		int streamId = (msg.hasStreamId()) ? msg.getStreamId() : 0;
//		
//		return new EndStreamW(streamId);
		
		return new EndStreamW();
	}
}
