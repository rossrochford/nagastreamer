package messages;

import protobuf.Messages.StatusMessage;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

public class StatusMessageW extends SerialisableMessage
{
	public static final int messageTypeNum = 3;
	
	public static final int GENERIC_STATUS_MESSAGE = 0;
	public static final int GENERIC_ERROR_MESSAGE = 1;
	public static final int TCP_CONNECTION_AUTH_SUCCESS = 2;
	public static final int TCP_CONNECTION_AUTH_FAIL = 3;
	public static final int DEVICE_UNIT_BUSY = 4;
	public static final int UNIT_NOT_LOCALLY_FOUND = 5;
	
	
	public int statusNum;
	public String message;
	
	public StatusMessageW(int statusNum)
	{
		this.statusNum = statusNum;
	}
	
	public StatusMessageW(int statusNum, String message)
	{
		this.statusNum = statusNum;
		this.message = message;
	}
	
	public static StatusMessageW createErrorMsg(String message)
	{
		return new StatusMessageW(GENERIC_ERROR_MESSAGE, message);
	}
	
	public static StatusMessageW createStatusMsg(String message)
	{
		return new StatusMessageW(GENERIC_STATUS_MESSAGE, message);
	}
	
	public byte[] toBytes()
	{
		protobuf.Messages.StatusMessage.Builder builder = protobuf.Messages.StatusMessage.newBuilder();
		
		builder.setStatusNum(statusNum);
		if(message != null)
			builder.setMessage(message);
		
		return _createBytes(messageTypeNum, builder);
	}
	
	public static StatusMessageW fromBytes(byte[] bytes)
	{
		StatusMessage msg = null;
		
		try {
			msg = protobuf.Messages.StatusMessage.parseFrom(ByteString.copyFrom(bytes, 1, bytes.length-1));
		} catch (InvalidProtocolBufferException e) {return null;}
		
		//String statusMessage = msg.hasMessage() ? msg.getMessage() : "";
		if(msg.hasMessage())
			return new StatusMessageW(msg.getStatusNum(), msg.getMessage());
		else
			return new StatusMessageW(msg.getStatusNum());
	}
}

