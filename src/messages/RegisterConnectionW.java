package messages;


public class RegisterConnectionW extends SerialisableMessage 
{
	public String clientContextUuid; //should really make this optional
	public String userUuid;
	
	public RegisterConnectionW(String userUuid)
	{
		this.userUuid = userUuid;
	}
	
	public RegisterConnectionW(String clientContextUuid, String userUuid)
	{
		this.clientContextUuid = clientContextUuid;
		this.userUuid = userUuid;
	}
	
	public byte[] toBytes() 
	{
	
		return null;
	}

}

/*message RegisterConnection
{
required string clientContextUuid = 1;
required string userUuid = 2;
}*/