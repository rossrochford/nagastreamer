package audio;

import java.util.List;

public class MultiVec 
{
	
	public float[][] vecs;
	public int numChannels;
	//int[] channelMapping; //if we were outputting to channels 7 & 8 we would want to address the channels as 0 & 1 <-- actually not necessary if we simply assign them to 'vecs' appropriately

	public MultiVec(int numChannels, int vecSize) //create a blank vector
	{
		this.numChannels = numChannels;
		this.vecs = new float[numChannels][vecSize];
	}
	
	public MultiVec(float[][] vecs) //when no channelNums are given we just assume the channels are sequential 
	{	
		this.vecs = vecs;
	}
	
	public MultiVec(float[][] vecs, int[] chanNums)
	{	
		vecs = new float[chanNums.length][];
		
		for(int i=0; i < chanNums.length; i++)
			this.vecs[i] = vecs[chanNums[i]];
	}
	
//	public void mul(double factor)  <-- not sure if I need this actually
//	{
//		for(int chan=0; chan<vecs.length; chan++)
//			for(int i=0; i<vecs[chan].length; i++)
//				vecs[chan][i] *= factor;
//	}
	
	public void mul(int pos, double factor) //usefule for fadeout code? 
	{
		for(int chan=0; chan<vecs.length; chan++)
			vecs[chan][pos] *= factor;
	}
}
