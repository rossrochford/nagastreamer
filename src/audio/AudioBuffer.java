package audio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import messages.RtAudioChunkW;

public abstract class AudioBuffer 
{	
	public abstract void addChunk(RtAudioChunkW chunk);
	
	public abstract RtAudioChunkW getChunk(int packetNum);
	
	public abstract void getVec(int playbackPos, MultiVec outVecs);

	public abstract int getMaxPacketNum();
}
