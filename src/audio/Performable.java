package audio;

public interface Performable 
{
	public void perform(float[][] in, float[][] out);
}
