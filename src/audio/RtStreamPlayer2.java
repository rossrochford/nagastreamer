package audio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.jitsi.impl.neomedia.pulseaudio.PA.context_success_cb_t;

import messages.RtAudioChunkW;
import messages.StartStreamW;
import serialisers.SampleDeserialiser;
import server.StreamSettings;
import util.Constants;
import util.Util;
import workerloops.DecompressionWorker;

public class RtStreamPlayer2 implements java.io.Serializable
{
	public final static int SAMPS_TO_REWIND_AFTER_UNACCEPTABLE_OVERSHOOTS = 400; //about 9 ms @ 48000
	public final static int INITIAL_PLAYBACK_OFFSET = (int)Util.msToSamps(200, true, AudioLoop.SAMPLE_RATE);  //2800; //this should depend on the sample rate
	public final static int NUM_SAMPS_BEHIND_FOR_SKIP = 1500;
	
	public int playbackPos;
	int targetPlaybackOffset;
	boolean playbackStarted = false;

	public volatile CircularAudioBuffer2 buffer;
	
//	SampleDeserialiser deserialiser;
	
	public StreamSettings ss;
	double chunkDur;
	int vecSize;
	
	List<Long> overshootEvents = new LinkedList<Long>();
	public boolean skipAtBeginning; //could consolidate these into 'skipsAllowed'
	public boolean skipsAllowedDuringPlayback;
	
	long currTime;
	
	public RtStreamPlayer2(CircularAudioBuffer2 audioBuffer, StreamSettings streamSettings, int vecSize)
	{
		this.buffer = audioBuffer;
		this.ss = streamSettings;
		this.targetPlaybackOffset = INITIAL_PLAYBACK_OFFSET;  //TODO: should never exceed window size of circular buffer 
		
		if(streamSettings.streamType == StartStreamW.RT_STREAM){
			skipAtBeginning = true;
			skipsAllowedDuringPlayback = true;
		}
		else if(streamSettings.streamType == StartStreamW.BOUNCE_STREAM){
			skipAtBeginning = false;
			skipsAllowedDuringPlayback = false;
		}
		
		this.chunkDur = Util.sampsToMs(ss.chunkSize); //more efficient to pre-compute this
		
		this.vecSize = AudioLoop.VEC_SIZE;
	}

	public void playVec(MultiVec outVecs)
	{	
		currTime = System.currentTimeMillis();
		
		if(playbackPos == 0 && buffer.maxPacketNum >= 2 && bufferSampLen() >= targetPlaybackOffset)
		{	
			if(skipAtBeginning){
				//buffer.setEstNextPacketTime(currTime+estimateNextPacketArrivalTime());
				int numSampsToSkip = (bufferSampLen()+numVirtualFutureSamps())-targetPlaybackOffset;				
				if(numSampsToSkip > 0)
					movePlaybackForwardBy(numSampsToSkip);
			}
			
			System.out.println("starting playback. Num future samps (including virtual): "+numFutureSamps(true)+" which should be == "+targetPlaybackOffset);
			playbackStarted = true;
		}
		
		if(!playbackStarted) //playback not started yet
			return;
		
		if(overshootStatsUnacceptable(3000, 3))
			rewindAfterOvershoot();
		
		if(skipsAllowedDuringPlayback && getPlaybackDeviation() < -NUM_SAMPS_BEHIND_FOR_SKIP){		
			fastforwardAndGetVec(outVecs);			
			return;
		}
		
		getVecAndMovePlaybackForward(outVecs);
		
		//applyFades(outVecs);    
	}
	
	public void getVecAndMovePlaybackForward(MultiVec outVecs)
	{
		buffer.getVec(playbackPos, outVecs);  //should we return a boolean so we can detect a blank vector (for restarting bounce streams)
		
		playbackPos += vecSize;
		
		if(playbackPos > buffer.maxPacketNum*ss.chunkSize){
			logOvershoot(currTime);
			Util.printIfDebug("overshoot at pos: "+playbackPos); 
		}		
	}
	
	private void logOvershoot(long overshootTimestamp)
	{
		while(overshootEvents.size() > 20)
			overshootEvents.remove(0);
		long lastOvershootTime = overshootEvents.size() > 0 ? overshootEvents.get(overshootEvents.size()-1) : -1;
		
		if(lastOvershootTime == -1 || overshootTimestamp-lastOvershootTime > 40)  //no need to log another overshoot if one has happened in the last 40ms
			overshootEvents.add(overshootTimestamp);
	}
	
	public void movePlaybackForwardBy(int numSamps)
	{
		System.out.println("skipping "+numSamps+" samples");
		playbackPos += numSamps;
		buffer.decompressUpTo(playbackPos);	
	}
	
	public void fastforwardAndGetVec(MultiVec outVecs)
	{
		int numSampsToSkip = getPlaybackDeviation()*-1;
		
		MultiVec fadeOutVec = new MultiVec(ss.numChannels, vecSize);
		getVecAndMovePlaybackForward(fadeOutVec);
		
		numSampsToSkip -= vecSize; //because we've just moved playbackPos forward by vecSize, we want to discount it here
		
		if(numSampsToSkip > 0)
			movePlaybackForwardBy(numSampsToSkip);
		
		playbackPos = (bufferSampLen()+numVirtualFutureSamps()) - targetPlaybackOffset;
		
		getVecAndMovePlaybackForward(outVecs);
		
		AudioUtil.crossfadeVecs(outVecs, outVecs, fadeOutVec);
		
		System.out.println("skipped playback forward by: "+(numSampsToSkip+vecSize)+" samples");
	}
	
	public void rewindAfterOvershoot()
	{
		targetPlaybackOffset += SAMPS_TO_REWIND_AFTER_UNACCEPTABLE_OVERSHOOTS;
		playbackPos = playbackPos - SAMPS_TO_REWIND_AFTER_UNACCEPTABLE_OVERSHOOTS;
		
		if(playbackPos < 0)
			playbackPos = 0;			
		
		overshootEvents.clear();
		
		System.out.println("RtStreamPlayer: rewound playback after unacceptable level of overshoots");
	}
	
	public double estimateNextPacketArrivalTime() //number of ms in the future
	{		
		LinkedList<Double> estimates = new LinkedList<Double>();
		
		synchronized(buffer){
			int maxPacketNum = buffer.maxPacketNum;
			int windowSize = Math.min(8, maxPacketNum);
			
			for(int i=1; i <= windowSize; i++){
				RtAudioChunkW currPrevChunk = buffer.getChunk(maxPacketNum-i);
				if(currPrevChunk == null)
					continue;
				double est = (i*chunkDur) - (currTime-currPrevChunk.arrivalTime);  //assumes currTime has already been updated in playVec
				estimates.add(est);
			}		
		}		
		
		return Util.averageList(estimates);
	}
	
	public boolean overshootStatsUnacceptable(int window, int overshootThreshold)
	{
		int numOvershoots = 0;
		
		for(int i=0; i<overshootEvents.size(); i++)
		{
			if(overshootEvents.get(i) < currTime-window)
				continue;
			numOvershoots++;
		}
		
		if(numOvershoots >= overshootThreshold){	
			overshootEvents.clear();
			return true;	
		}
		
		return false;
	}	
	
	public int bufferSampLen()
	{
		return buffer.maxPacketNum*ss.chunkSize;
	}
	
	public int numVirtualFutureSamps()
	{
		return (int) Util.msToSamps(chunkDur-estimateNextPacketArrivalTime(), true);
	}

	public int numFutureSamps(boolean includeVirtualSamps)
	{
		if(includeVirtualSamps)
			return (bufferSampLen() - playbackPos) + numVirtualFutureSamps();
		else
			return bufferSampLen() - playbackPos;
	}
	
	public int getPlaybackDeviation()
	{
		return targetPlaybackOffset - numFutureSamps(true); //positive = ahead, negative = behind
	}
	
	public int buffPos(int samplePos)
	{
		return samplePos / ss.chunkSize;
	}
}
