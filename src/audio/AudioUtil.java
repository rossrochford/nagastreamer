package audio;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

public class AudioUtil 
{
	public static byte[] floatToLittleEndian16Bit(float[][] samples)
	{
		int numChannels = samples.length;
		int chunkSize = samples[0].length;
		
		byte[] outputBuf = new byte[chunkSize*2*numChannels];
		
		return floatToLittleEndian16Bit(samples, outputBuf);
	}
	
	public static byte[] floatToLittleEndian16Bit(float[][] samples, byte[] outputBuf)
	{
		int numChannels = samples.length;
		int chunkSize = samples[0].length;
		
		if(numChannels == 1)
		{
			for(int i=0; i<chunkSize; i++)
	        {
	            short shortVal = (short)(samples[0][i]*32767.0);
	            outputBuf[(i*2)] = (byte)shortVal;
	            outputBuf[(i*2)+1] = (byte)(shortVal >> 8);
	        }
		}
		else if(numChannels == 2)
		{
			for(int i=0; i<chunkSize; i++)
	        {
	            short shortValL = (short)(samples[0][i]*32767.0);
	            outputBuf[(i*4)] = (byte)shortValL;
	            outputBuf[(i*4)+1] = (byte)(shortValL >> 8);
	            
	            short shortValR = (short)(samples[1][i]*32767.0);
	            outputBuf[(i*4)+2] = (byte)shortValR;
	            outputBuf[(i*4)+3] = (byte)(shortValR >> 8);
	        }
		}
		else{
			System.out.println("Error: unexpected number of channels");
			return null;
		}
		
		return outputBuf;
	}
	
	public static float[][] littleEndian16BitToFloat(byte[] bytes, int numChannels)
	{
		int chunkSize = (bytes.length/2)/numChannels; //numChannels*2 bytes per sample
		float[][] sampleFloats = new float[numChannels][chunkSize]; 
        
        if(numChannels == 1)        
        	for(int k=0; k<chunkSize; k++){
        		short shortVal = (short) ( ((((short)bytes[(k*2)+1])<<8)) | (bytes[(k*2)] & 0xFF) );               
                sampleFloats[0][k] = (float)(shortVal/32767.0);
            }
        else if(numChannels == 2)
        	for(int k=0; k<chunkSize; k++){
        		short shortValL = (short) ( ((((short)bytes[(k*4)+1])<<8)) | (bytes[(k*4)] & 0xFF) );               
                sampleFloats[0][k] = (float)(shortValL/32767.0);                
                short shortValR = (short) ( ((((short)bytes[(k*4)+3])<<8)) | (bytes[(k*4)+2] & 0xFF) );               
                sampleFloats[1][k] = (float)(shortValR/32767.0);
        	}
        else{
        	System.out.println("invalid num channels");
        	return null;
        }
        
        return sampleFloats;
	}
	
	public static float[][] bigEndian16BitToFloat(byte[] bytes, int numChannels)
	{
		int chunkSize = (bytes.length/2)/numChannels; //numChannels*2 bytes per sample
		float[][] sampleFloats = new float[numChannels][chunkSize]; 
        
        if(numChannels == 1)        
        	for(int k=0; k<chunkSize; k++){
        		short shortVal = (short) ( ((((short)bytes[(k*2)])<<8)) | (bytes[(k*2)+1] & 0xFF) );               
                sampleFloats[0][k] = (float)(shortVal/32767.0);
            }
        else if(numChannels == 2)
        	for(int k=0; k<chunkSize; k++){
        		short shortValL = (short) ( ((((short)bytes[(k*4)])<<8)) | (bytes[(k*4)+1] & 0xFF) );               
                sampleFloats[0][k] = (float)(shortValL/32767.0);                
                short shortValR = (short) ( ((((short)bytes[(k*4)+2])<<8)) | (bytes[(k*4)+3] & 0xFF) );               
                sampleFloats[1][k] = (float)(shortValR/32767.0);
        	}
        else{
        	System.out.println("invalid num channels");
        	return null;
        }
        
        return sampleFloats;
	}
	
	 public static float[][] seg(float[][] a, int startPos, int len)
	 {	
		 float[][] chunk = new float[a.length][a[0].length];
			for(int chan=0; chan<a.length; chan++)
				System.arraycopy(a[chan], startPos, chunk[chan], 0, len);
		return chunk;
	 }
	 
    public static float[][] loadAudioFile(String filename) //TODO: add option to resample audio to the present sample rate
    {
    	final int BUFFER_SIZE = 128000;
        File soundFile;
        AudioInputStream audioStream;
        AudioFormat audioFormat;
        int MAX_SIZE = 8500000; //8.5MB;
    	
        try {
            soundFile = new File(filename);
            audioStream = AudioSystem.getAudioInputStream(soundFile);
        } catch (Exception e) {e.printStackTrace(); System.exit(1); return null;}

        audioFormat = audioStream.getFormat();
        
        if(audioFormat.getSampleSizeInBits() != 16){
        	System.out.println("error: only accept audio files with 16-bit samples"); //note: wav is little-endian, aif is big-endian
        	return null;
        }

        int nBytesRead = 0;  byte[] abData = new byte[BUFFER_SIZE];
        byte[] totalBytes = new byte[MAX_SIZE]; int byteCount = 0;
        while (nBytesRead != -1) 
        {
            try {
                nBytesRead = audioStream.read(abData, 0, abData.length);
            } catch (IOException e) {e.printStackTrace();}
            
            if(nBytesRead >=0){
            	System.arraycopy(abData, 0, totalBytes, byteCount, nBytesRead);
            	byteCount += nBytesRead;
            }
            
            if(byteCount >= MAX_SIZE)
            	System.out.println("warning: cannot load files > 9MB");
        }
        
        //float[][] samps = new float[audioFormat.getChannels()][(byteCount/audioFormat.getChannels())/2]; //2 bytes per sample
        if(audioFormat.isBigEndian())
        	return AudioUtil.bigEndian16BitToFloat(totalBytes, audioFormat.getChannels());
        else
        	return AudioUtil.littleEndian16BitToFloat(totalBytes, audioFormat.getChannels());
     }
	    
	 public static void multiArrayCopy(float[][] src, int srcStart, float[][] dest, int destStart, int len)
	 {
		 assert(src.length  == dest.length);
		 
		 for(int chan=0; chan<src.length; chan++)
			 System.arraycopy(src[chan], srcStart, dest[chan], destStart, len);
	 }
	 
	 public static void blankVec(float[][] outVec)
	 {
		 for(int chan=0; chan<outVec.length; chan++)
			 for(int i=0; i< outVec[0].length; i++)
				 outVec[chan][i] = 0.0f;
	 }
	 
	public static void crossfadeVecs(MultiVec resultVec, MultiVec fadeinVec, MultiVec fadeoutVec)
	{
		int vecSize = resultVec.vecs[0].length;
		int numChannels = resultVec.vecs.length;
		
		//do quick cross-fade
		double crossFadeIncr = 1.0/vecSize;
		for(int chan=0; chan < numChannels; chan++){	
			double fadeoutVal = 1.0; double fadeinVal = 0.0;
			for(int i=0; i < vecSize; i++){
				resultVec.vecs[chan][i] = (float)((fadeoutVec.vecs[chan][i] * fadeoutVal) + (fadeinVec.vecs[chan][i] * fadeinVal));
				fadeinVal += crossFadeIncr; fadeoutVal -= crossFadeIncr;
			}
		}
	}
}
