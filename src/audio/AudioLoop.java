package audio;
import java.util.LinkedList;
import java.util.List;

import javax.sound.sampled.AudioFormat;

import org.jaudiolibs.beads.AudioServerIO;

import util.Constants;
import util.Util;
import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.core.AudioIO;
import net.beadsproject.beads.core.IOAudioFormat;
import net.beadsproject.beads.core.UGen;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.ugens.Function;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Noise;
import net.beadsproject.beads.ugens.SamplePlayer;
import net.beadsproject.beads.ugens.WavePlayer;


//note: if we use JNA with jack, its highly recommended to use JNA 3.4.0+

public abstract class AudioLoop 
{
	public static final int VEC_SIZE = 256; //NB: this needs to be the same as the setting for Jack
	public static final int TOTAL_CHANNELS = 2;
	public static final int SAMPLE_RATE = 48000;//48000;
	public static boolean HARDWARE = true;
	public static boolean ENABLE_STEREO_TEST_MODE = false;
	
	//Default audio settings on a mac are: PCM_SIGNED 44100.0 Hz, 16 bit, stereo, 4 bytes/frame, big-endian. To check do: System.out.println(AudioContext.defaultAudioFormat(AudioLoop.TOTAL_CHANNELS));
	private static boolean isSigned = true;
	private static boolean isBigEndian = true;
	private static int bytesPerSample = 2; //actually 4 (32-bit) seems to work!
	private static int bitsPerSample = 8*bytesPerSample;
	private static int frameSize = TOTAL_CHANNELS*bytesPerSample; //number of bytes per sample over all channels (see: http://www.jsresources.org/faq_audio.html#frame_size)
	private static int frameRate = SAMPLE_RATE; //not a straight-forward parameter (see http://www.jsresources.org/faq_audio.html#frame_rate), but we can just set it to the sample rate
	
	public static AudioFormat HARDWARE_AUDIO_FORMAT = null; //now set in start audio. old: new AudioFormat(SAMPLE_RATE, bitsPerSample, TOTAL_CHANNELS, isSigned, isBigEndian); //or: new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, SAMPLE_RATE, bytesPerSample*8, TOTAL_CHANNELS, frameSize, frameRate, isBigEndian)); or: AudioContext.defaultAudioFormat(AudioLoop.TOTAL_CHANNELS)); or: new AudioFormat(SAMPLE_RATE, bytesPerSample*8, TOTAL_CHANNELS, isSigned, isBigEndian);
	
	protected volatile static AudioLoop instance;
	
	protected volatile LinkedList<Performable> performables = new LinkedList<Performable>();
	protected volatile boolean isRunning = false;
	
	public abstract void startAudio();
	
	public AudioFormat getHardwareAudioFormat(int numChannels)
	{
		return new AudioFormat(HARDWARE_AUDIO_FORMAT.getSampleRate(), HARDWARE_AUDIO_FORMAT.getSampleSizeInBits(), numChannels, isSigned, HARDWARE_AUDIO_FORMAT.isBigEndian());
	}
	
	public void register(Performable p)
	{
		performables.add(p);
	}
	
	public void unregister(Performable p)
	{
		for(int i=0; i<performables.size(); i++)
			if(performables.get(i).equals(p)){
				performables.remove(i);
				break;
			}
	}
	
	public static AudioLoop getInstance()
	{
		if(instance == null)
		{
			if(HARDWARE)
				instance = new HardwarePerformer();
			else
				instance = new SoftwarePerformer();
		}
		return instance;
	}

	
	public static class SoftwarePerformer extends AudioLoop implements Runnable
	{		
		float[][] prevVec = new float[AudioLoop.TOTAL_CHANNELS][AudioLoop.VEC_SIZE];
		double interVecDelay = ((AudioLoop.VEC_SIZE+0.0)/AudioLoop.SAMPLE_RATE)*1000000000.0; //ms
		
		public void startAudio()
		{
			if(isRunning)
				return;
			isRunning = true;
			
			new Thread(this).start();
		}

		public void run() 
		{
			int iterCount = 0;
			long startTime = System.nanoTime();
			while(isRunning)
			{
				float[][] out = new float[AudioLoop.TOTAL_CHANNELS][AudioLoop.VEC_SIZE];
				try{
					masterPerform(prevVec, out);
				}catch(Exception e){System.out.println("masterPerform() threw expection: "+e);}
				
				prevVec = out; //simulates hardware pass-through
				
				double oughtToBe = startTime + (iterCount*interVecDelay);
				iterCount++;				
				double diff = System.nanoTime()-oughtToBe;		
				
				Util.sleep(Math.round(((interVecDelay-diff)/1000000.0)));
				
				//System.out.println("ought: "+oughtToBe);System.out.println("diff: "+diff/1000000.0);
			}
			isRunning = false;
		}
		
		public void masterPerform(float[][] in, float[][] out)
		{
			for(int i=0; i < performables.size(); i++)
				performables.get(i).perform(in, out);
		}
		
//		public void masterPerform() //simulates hardware pass-through
//		{			
//			float[][] out = new float[AudioLoop.TOTAL_CHANNELS][AudioLoop.VEC_SIZE];
//			for(int i=0; i < performables.size(); i++)
//				performables.get(i).perform(prevVec, out);			
//			prevVec = out;
//		}		
	}
	
//	public static class MyAudioContext extends AudioContext
//	{
//		public MyAudioContext(int vecSize, AudioFormat f)
//		{
//			super(vecSize, f);
//		}
//		
//		public static AudioIO defaultAudioIO() {
//			return new JavaSoundAudioIO(800);
//		}
//	}
	
	public static class HardwarePerformer extends AudioLoop
	{
		AudioContext ac;
		long startTime;
		
		public synchronized void startAudio() 
		{
			startTime = System.currentTimeMillis();
			
			if(isRunning)
				return;
			isRunning = true;
			
			if(ac == null) 
			{	// should we be reinitialising this each time?
				IOAudioFormat defFormat = AudioContext.defaultAudioFormat(TOTAL_CHANNELS,TOTAL_CHANNELS); //I presume it follows Jack's lead?
				IOAudioFormat audioFormat = new IOAudioFormat(SAMPLE_RATE, bitsPerSample, TOTAL_CHANNELS, TOTAL_CHANNELS, defFormat.signed, defFormat.bigEndian); //new IOAudioFormat(float sampleRate, int bitDepth, int inputs, int outputs, boolean signed, boolean bigEndian) 
				HARDWARE_AUDIO_FORMAT = new AudioFormat(audioFormat.sampleRate, audioFormat.bitDepth, audioFormat.outputs, audioFormat.signed, audioFormat.bigEndian);
				System.out.println("HARDWARE_AUDIO_FORMAT: "+HARDWARE_AUDIO_FORMAT);
				ac = new AudioContext(new AudioServerIO.Jack(), VEC_SIZE, audioFormat); //old: ac = new AudioContext(VEC_SIZE, HARDWARE_AUDIO_FORMAT);
			}
			else
				return; //not sure yet about what should happen if startAudio is called twice
			
			System.out.println("finished AC creation");
				
			UGen h = new UGen(ac, HARDWARE_AUDIO_FORMAT.getChannels(), HARDWARE_AUDIO_FORMAT.getChannels()) {
				public void calculateBuffer() { //replace with: bufOut[chan][0] = bufIn[chan][0]; for a quick audio test
					try{
						masterPerform(bufIn, bufOut); 
					}
					catch(Exception e){System.out.println("Caught RuntimeException: ");e.printStackTrace();}
				}
			};
			
			//something like this!
			h.addInput(ac.getAudioInput());
			ac.out.addInput(h);
			ac.start();
			
			System.out.println("started audio hardware");
		}
		
		float[][][] tempOut = new float[22][TOTAL_CHANNELS][VEC_SIZE];
		private void masterPerform(float[][] in, float[][] out) throws Exception   
		{
			LinkedList<Performable> _performables = (LinkedList<Performable>) performables.clone();
			
			if(ENABLE_STEREO_TEST_MODE) //TODO: replicate this in SoftwarePerformer
			{
				if(AudioLoop.TOTAL_CHANNELS != 2)
					System.out.println("Warning: AudioLoop.TOTAL_CHANNELS should be == 2 when ENABLE_STEREO_TEST_MODE == true");
				
				if(_performables.size() == 0)
					return;		
				
				//TODO: ensure the performables.size() doesn't change (perhaps by copying it?)
				for(int i=0; i < _performables.size(); i++)
					_performables.get(i).perform(in, tempOut[i]);

				float scaleVal = (float) 1.0/_performables.size();
				//scaleVal *= 0.2; //<--temporary so I can listen to music while testing!
				for(int i=0; i < _performables.size(); i++)
					for(int chan=0; chan < out.length; chan++)
						for(int j=0; j<out[0].length; j++){
							if(i == 0)
								out[chan][j] = 0.0f;
							out[chan][j] += tempOut[i][chan][j]*scaleVal;
						}
				
				/*
				 * could also module volume of each performer, something like this:
				 if(modulateVolume)
					for(int i=0; i<out[0].length; i++)
					{ //we're adding a fade in/out to make multiple performers more audible
						for(int chan=0; chan<out.length; chan++)
							out[chan][i] = out[chan][i]*fadeVal;  //will need a separate fadeVal for each performer (use an array)
							fadeVal += fadeIncr;  
							if(fadeVal <= 0.0 || fadeVal >= 1.0)
								fadeIncr *= -1.0;
					}
				 */
			}
			else 
				for(int i=0; i < _performables.size(); i++)
					_performables.get(i).perform(in, out);
			
			if(Math.random() < 0.001){
				System.out.println("free memory: "+Runtime.getRuntime().freeMemory()/1048576+"MB");
				System.out.println("total memory: "+Runtime.getRuntime().totalMemory()/1048576+"MB");
				System.out.println("allocated memory: "+(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory())/1048576+"MB");
				System.out.println("run time: "+(System.currentTimeMillis()-startTime)/1000.0);
				System.out.println("---------------------------------------------------------------");
			}
		}		
		
//		public void startAudio()
//		{
//			ac = new AudioContext(AudioContext.defaultAudioFormat(AudioLoop.TOTAL_CHANNELS)); // or should we be reinitialising this each time?
//			WavePlayer wp = new WavePlayer(ac, 440, Buffer.SINE);
//			ac.out.addInput(wp);
//			ac.start();
//		}
		
//		public void startAudio()
//		{
//			String audioFile = selectInput();// This tells the program to ask for a file called audioFile
//			//The next line creates a new sample player called player, connects it to the audio context
//			//and loads the sample called audioFile
//			SamplePlayer player = new SamplePlayer(ac, SampleManager.sample(audioFile));
//			//The rest is the same as before, although youre now plugging in your sample player
//			Gain g = new Gain(ac, 2, 0.2); // now we have 2 channels, not 1.
//			g.addInput(player);
//			ac.out.addInput(g);
//			ac.start();
//		}
		
		public void stopAudio()
		{
			if(isRunning || ac != null)
				ac.stop();
			isRunning = false;
		}

		public class TestUGen extends UGen
		{
			public TestUGen(AudioContext arg0, int numInputs, int numOutputs) {
				super(arg0, numInputs, numOutputs);
			}

			@Override
			public void calculateBuffer() 
			{
				//System.out.println("calculateBuffer() called");
				
				if(!isRunning) 
					kill();
				
				for(int chan=0; chan < bufOut.length; chan++)
					for(int i=0; i<bufOut[chan].length; i++)
						bufOut[chan][i] = (float)((Math.random()*2.0)-1.0);
			}			
		}				
		
		public class HardwareUgen extends UGen
		{
			List<Performable> performables;
			
			public HardwareUgen(AudioContext ac, List<Performable> performables) 
			{
				super(ac);				
				this.performables = performables;
			}

			@Override
			public void calculateBuffer() 
			{
				System.out.println("calculateBuffer() called");
				
				for(int i=0; i<performables.size(); i++)
					performables.get(i).perform(bufIn, bufOut);	
			}			
		}		
	}
}
	
//	public class HardwareUgen extends UGen implements Performer
//	{	
//		LinkedList<Performable> performables = new LinkedList<Performable>();
//		
//		public HardwareUgen(AudioContext arg0) {
//			super(arg0);
//		}
//		
//		public void calculateBuffer() 
//		{			
//			
//		}
//
//		public void register(Performable p) 
//		{
//			performables.add(p);			
//		}		
//	}


/*
public static void main(String[] args)
{		
	AudioContext ac = new AudioContext(AudioContext.defaultAudioFormat(2)); 
	//you can also do: AudioContext ac = new AudioContext(AudioContext.defaultAudioFormat(0, 8));
	
//	new AudioContext(new AudioServerIO.Jack(), bufferSize, new
//			IOAudioFormat(sampleRate, bitDepth, inputs, outputs));
//	
//	new AudioContext(new AudioServerIO.Jack(), 512, new IOAudioFormat(44100, 16, 8, 6));

	System.out.println("no. of inputs:  " + ac.getAudioInput().getOuts());
	System.out.println("no of outputs: " + ac.out.getIns()); 
	
	Noise n = new Noise(ac);
	Gain g = new Gain(ac, 1, 0.1f);

	g.addInput(n);
	ac.out.addInput(g);
	ac.start();
}*/