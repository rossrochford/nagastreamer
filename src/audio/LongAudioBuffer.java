package audio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import serialisers.OpusDecoder;
import serialisers.SampleDeserialiser;
import server.StreamSettings;
import util.Int;
import workerloops.DecompressionWorker;
import workerloops.DecompressionWorker.DecompressionTask2;

import com.sun.jna.platform.win32.WinDef.CHAR;

import messages.RtAudioChunkW;

public class LongAudioBuffer extends AudioBuffer
{
	final static int DECOMPRESSION_LOOKAHEAD = 900;
	
	int windowChunkSize;
	volatile ArrayList<RtAudioChunkW> chunkBuf;
	int chunkSize;
	int maxPacketNum = 0;
	
	int windowFloatSize;
	float[][] floatBuf;
	
	OpusDecoder dec;	
	
	volatile int lastPacketScheduledForDecompression;
	
	
	public LongAudioBuffer(ArrayList<RtAudioChunkW> data, StreamSettings ss)  //TODO: RtPlayer should be constrained so that its playbackOffset never exceeds the buffer's window size
	{
		this.windowChunkSize = data.size();
		this.chunkSize = ss.chunkSize;
		
		chunkBuf = data;
		for(int i=0; i<chunkBuf.size(); i++)
			chunkBuf.add(null);
		
		windowFloatSize = windowChunkSize*chunkSize;
		floatBuf = new float[ss.numChannels][windowFloatSize];		
		
		this.dec = (OpusDecoder) SampleDeserialiser.getDecoder(ss.compType, ss.numChannels, ss.sampleRate, ss.chunkSize);
	}
	
	public synchronized void addChunk(RtAudioChunkW chunk)  //synchronizing for RtPlayer.estimateNextPacketArrivalTime() doesn't have problems 
	{
		int chunkBufPos = chunk.packetNum % windowChunkSize;
		
		if(chunkBuf.get(chunkBufPos) == null || chunk.packetNum >= chunkBuf.get(chunkBufPos).packetNum)
			chunkBuf.set(chunkBufPos, chunk);
		
		//if(chunk.packetNum == lastDecompressedPacket.value-1)
			//scheduleDecompression(chunk.packetNum, chunk);  //<--TODO: consider replacing this
		
		if(chunk.packetNum > maxPacketNum)
			maxPacketNum = chunk.packetNum;
	}
	
	public RtAudioChunkW getChunk(int packetNum)
	{	
		if(packetNum > maxPacketNum)
			return null;		
		RtAudioChunkW chunk = chunkBuf.get(packetNum % windowChunkSize);
		
		if(chunk == null || chunk.packetNum != packetNum)
			return null;
		return chunk;			
	}
	
	public void getVec(int playbackPos, MultiVec outVec)
	{
		int packetNum = playbackPos / chunkSize;
		
		if(getChunk(packetNum) == null || getChunk(packetNum).state != 'D')
			AudioUtil.blankVec(outVec.vecs);
			//TODO: store flag to remember to fade-in
		else{
			int floatBufPos = playbackPos % windowFloatSize;
			
			if(floatBufPos+AudioLoop.VEC_SIZE <= windowFloatSize)
				AudioUtil.multiArrayCopy(floatBuf, floatBufPos, outVec.vecs, 0, AudioLoop.VEC_SIZE);
			else{
				//TODO: look at the old implementation of RtPlayer.getVecAndMovePlaybackForward() for inspiration on this
				AudioUtil.multiArrayCopy(floatBuf, floatBufPos, outVec.vecs, 0, windowFloatSize-floatBufPos); 
				int numRemainingSamps = AudioLoop.VEC_SIZE-(windowFloatSize-floatBufPos); //1
				int vecPos = windowFloatSize-floatBufPos; 
				floatBufPos = 0;				
				try{
					AudioUtil.multiArrayCopy(floatBuf, floatBufPos, outVec.vecs, vecPos, numRemainingSamps);
				} catch(ArrayIndexOutOfBoundsException e){
					System.out.println("floatBufPos: "+floatBufPos);
					System.out.println("vecPos: "+vecPos);
					System.out.println("numRemainingSamps "+numRemainingSamps);					
					throw e;
				}
			}	
		}
		
		decompressUpTo(playbackPos+AudioLoop.VEC_SIZE+DECOMPRESSION_LOOKAHEAD);
		
		//TODO: add fadeout if next vec is expected to be blank
	}
	
	public void decompressUpTo(int playbackPos)
	{	//playback has reached a point, so the decompression needs to catch up
		
		int packetNum = playbackPos / chunkSize;
		
		for(int i=lastPacketScheduledForDecompression+1; i<=packetNum; i++){
			RtAudioChunkW currChunk = getChunk(i);
			if(currChunk == null || currChunk.state == 'C')
				scheduleDecompression(i, currChunk);		
		}
	}
	
	private synchronized void scheduleDecompression(int packetNum, RtAudioChunkW chunk)
	{		
		//if(packetNum < maxPacketNum-windowChunkSize)
			//return; //may happen occasionally if a cutting-edge chunk comes in and playback turns out to be way behind
		
		if(chunk == null) // && chunkBuf.get(packetNum-1) != null || chunkBuf.get(packetNum-2) != null
			chunk = new RtAudioChunkW(packetNum, null, 0);
		
		chunk.state = 'Q';
		lastPacketScheduledForDecompression = packetNum;
		
		int floatBufPos = (packetNum % windowChunkSize)*chunkSize;
		DecompressionTask2 task = new DecompressionTask2(chunk, dec, floatBuf, floatBufPos);
		DecompressionWorker.getInstance().registerTask(task, true);
	}
	
	public int getMaxPacketNum() 
	{
		return maxPacketNum;
	}
}
