package mess;

import workerloops.DecompressionWorker;

public class TestBed 
{
	public static void sleep(int ms)
	{
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {e.printStackTrace();}
	}
	
	public static void testDecompressionWorker()
	{
		DecompressionWorker decWorker = DecompressionWorker.getInstance();
		decWorker.start();
		
		Runnable r = new Runnable(){
			public void run() {
				System.out.println("starting task");
				sleep(1000);
				System.out.println("finished task");
			}
		};
		
		sleep(1000);
		
		System.out.println("registering task 1");
		decWorker.registerTask(r, false);
		
		decWorker.unblockIfNecessary();
		
		System.out.println("sleeping main thread for 2s");
		sleep(2000);
		
		decWorker.unblockIfNecessary();
		System.out.println("registering task 2");
		decWorker.registerTask(r, false);
		decWorker.unblockIfNecessary();
		
		System.out.println("sleeping main thread for 1s");
		sleep(1000);
		
		System.out.println("registering task 3");
		decWorker.registerTask(r, false);
		
		
		System.out.println("sleeping main thread for 1s");
		sleep(1000);		
		decWorker.unblockIfNecessary();
		
		System.out.println("stopping");
		decWorker.stop();
	}
	
	public static void main(String[] args)
	{
		testDecompressionWorker();
	}
}
