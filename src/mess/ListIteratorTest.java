package mess;

import java.util.LinkedList;
import java.util.ListIterator;

public class ListIteratorTest 
{
	public static void main(String[] args)
	{
		LinkedList<Integer> l = new LinkedList<Integer>();
		l.add(1); 
		l.add(2);
		l.add(3);
		l.add(4);
		
		System.out.println("backwards items");
		ListIterator<Integer> iter = l.listIterator(l.size());
		while(iter.hasPrevious())
			System.out.println(iter.previous());
	}
}
