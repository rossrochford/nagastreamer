package workerloops;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import messages.RtAudioChunkW;
import serialisers.OpusDecoder;
import serialisers.SampleDeserialiser;
import util.Int;

public class DecompressionWorker extends WorkerLoop 
{
	
	static DecompressionWorker instance; //we can't use WorkerLoops instance!
	
	ConcurrentLinkedQueue<Runnable> taskQueue = new ConcurrentLinkedQueue<Runnable>();
	
	//TODO: LOG TIME BETWEEN ADDING TO QUEUE AND DECOMPRESSION COMPLETION
	
	public static DecompressionWorker getInstance() //may not be necessary because we'll just be using the parent's interface
	{
		if(instance == null)
			instance = new DecompressionWorker();
		return instance;
	}
	
//	public void queueChunk(RtAudioChunkW chunk, SampleDeserialiser dec)
//	{
//		queueChunk(chunk, dec, false);
//	}
	
	public void queueChunk(RtAudioChunkW chunk, SampleDeserialiser dec, boolean inAudioThread)
	{	
		DecompressionTask task = new DecompressionTask(chunk, dec);
		this.registerTask(task, inAudioThread);
		
//		long t = System.currentTimeMillis();
//		System.out.println("queueChunk called for chunk "+chunk.packetNum+" at time: "+System.currentTimeMillis());
	}
	
	//TODO: make this implement a Task interface (or could use runnable)
	public static class DecompressionTask implements Runnable
	{
		public SampleDeserialiser deserialiser;
		public RtAudioChunkW chunk;
		
		public DecompressionTask(RtAudioChunkW chunk, SampleDeserialiser deserialiser)
		{
			this.chunk = chunk;
			this.deserialiser = deserialiser;
		}

		public void run() 
		{
			//deserialiser.deserialiseChunk(chunk);	//might be better to change this so that the samples are passed to deserialiser and set in the chunk here, thus decoupling deserialisation from knowledge of chunks		
			if(chunk.samples == null && chunk.audioPayload != null){
				chunk.samples = deserialiser.deserialiseSamples(chunk.audioPayload);
				//chunk.audioPayload = null; //to save memory (not sure if its worth it)
			}
		}
	}
	
	public static class DecompressionTask2 implements Runnable
	{
		public OpusDecoder deserialiser;  //temporary: should be: SampleDeserialiser
		public RtAudioChunkW chunk;
		public float[][] floatBuf;
		public int floatBufPos;
		
		public DecompressionTask2(RtAudioChunkW chunk, OpusDecoder deserialiser, float[][] floatBuf, int floatBufPos)
		{
			this.chunk = chunk;
			this.deserialiser = deserialiser;
			this.floatBuf = floatBuf;
			this.floatBufPos = floatBufPos;
		}

		public void run() 
		{
			//note: chunk.audioPayload could be null
			deserialiser.deserialiseSamples(chunk.audioPayload, chunk.audioPayloadOffset, floatBuf, floatBufPos);
			chunk.state = 'D';
			//lastDecompressedPacket.value = chunk.packetNum;
		}
		
		/*
		 * Runnable r = new Runnable() {
			public void run() {
				if(packetNum != lastDecompressedPacket-1)
					return;
				int floatBufPos = (packetNum % windowChunkSize)*chunkSize;  //(packetNum*chunkSize)%windowFloatSize;
				dec.deserialiseSamples(chunk != null ? chunk.audioPayload : null, chunk.audioPayloadOffset, floatBuf, floatBufPos);
				lastDecompressedPacket = packetNum;
			}
		};
		 */
	}

	public ConcurrentLinkedQueue<Runnable> getTaskQueue() 
	{
		return taskQueue;
	}
}
