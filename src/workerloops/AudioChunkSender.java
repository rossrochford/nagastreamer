package workerloops;

import java.util.concurrent.ConcurrentLinkedQueue;

import serialisers.OpusEncoder;
import serialisers.SampleSerialiser;
import server.CloudioConnection;
import messages.RtAudioChunkW;

public class AudioChunkSender extends WorkerLoop
{
	static AudioChunkSender instance;
	private ConcurrentLinkedQueue<Runnable> taskQueue = new ConcurrentLinkedQueue<Runnable>();
	
	public static AudioChunkSender getInstance()
	{
		if(instance == null)
			instance = new AudioChunkSender();
		return instance;
	}	
	
	public static class AudioChunkSendTask implements Runnable
	{
		public RtAudioChunkW chunk;
		SampleSerialiser serialiser;
		CloudioConnection destConnection;
		
		public AudioChunkSendTask(CloudioConnection destConnection, RtAudioChunkW chunk, SampleSerialiser serialiser)
		{
			this.destConnection = destConnection;
			this.chunk = chunk;
			this.serialiser = serialiser;
		}		
		
		public void run() 
		{
			chunk.serialiseSamples(serialiser);
			destConnection.sendMsg(chunk);
		}		
	}
	
	public static class AudioChunkSendTask2 implements Runnable
	{
		public RtAudioChunkW chunk;
		OpusEncoder serialiser;
		CloudioConnection destConnection;
		
		public AudioChunkSendTask(CloudioConnection destConnection, RtAudioChunkW chunk, OpusEncoder serialiser)
		{
			this.destConnection = destConnection;
			this.chunk = chunk;
			this.serialiser = serialiser;
		}		
		
		public void run() 
		{
			chunk.serialiseSamples(serialiser);
			destConnection.sendMsg(chunk);
		}		
	}
	
	public ConcurrentLinkedQueue<Runnable> getTaskQueue() 
	{
		return taskQueue;
	}


}
