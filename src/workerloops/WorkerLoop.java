package workerloops;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import messages.RtAudioChunkW;
import serialisers.SampleDeserialiser;

public abstract class WorkerLoop implements Runnable 
{
	//static WorkerLoop instance;
	
	boolean isRunning = false;
	boolean waiting = false;
	//private ConcurrentLinkedQueue<Runnable> taskQueue = null;//new ConcurrentLinkedQueue<Runnable>();
	
	Lock lock = new ReentrantLock();
	Condition isNotEmpty = lock.newCondition();
	volatile boolean isBlocked = false;
	
	public abstract ConcurrentLinkedQueue<Runnable> getTaskQueue();
	
	
	public static WorkerLoop getInstance() //ideally we would mark this as abstract but the lannguage doesn't allow it
	{
		System.out.println("WARNING: WorkerLoop.getInstance() should never be called");
		
		return null;
	}
	
	public void start() //TODO: could allow for multiple threads, or processes 
	{
		if(isRunning)
			return;
		
		Thread t = new Thread(this);
		t.start();
	}
	
	public void registerTask(Runnable task, boolean inAudioThread)
	{
		this.getTaskQueue().add(task); // or offer()?
		
		if(inAudioThread == false && isBlocked)
			signalNotEmpty();
	}
	
	private void signalNotEmpty()
	{
		lock.lock();
		isNotEmpty.signalAll(); //isNotEmpty.notifyAll();
		lock.unlock();
	}
	
	
	public void unblockIfNecessary() //should be called once at the end of the audio callback (perhaps not even every time!)
	{
		if(!getTaskQueue().isEmpty() && isBlocked)
			signalNotEmpty();
	}
	
	public void run()
	{
		long lastSpinTime = System.currentTimeMillis();
		
		isRunning = true;
		while(isRunning)
		{
			Runnable task = getTaskQueue().poll();
			
			if(task == null) //while means the queue is empty right?
			{
				isBlocked = true;
				//System.out.println("blocking loop");
				lock.lock(); //not sure if these locks are necessary, perhaps only when there are multiple worker threads
				
				isNotEmpty.awaitUninterruptibly();
				
				//System.out.println("loop unblocked");
				isBlocked = false;
			}
			else
			{
				//System.out.println("spin (time interval: "+(System.currentTimeMillis()-lastSpinTime)+")");
				lastSpinTime = System.currentTimeMillis();
				
				task.run();
			}			
		}
		
		System.out.println("worker thread ended");
	}
	
	public void stop()
	{
		isRunning = false;
		signalNotEmpty(); //not because its not empty but to unblock the loop if its currently waiting
	}
}
