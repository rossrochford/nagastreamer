package workerloops;

import java.net.SocketAddress;
import java.util.HashMap;

import auth.UserManager;
import messages.RegisterConnectionW;
import messages.RtAudioChunkW;
import messages.SerialisableMessage;
import server.ClientContext;
import server.DatagramMsgChannel;
import util.Pair;

public class UdpReceiveLoop implements Runnable
{
	final static int receivePort = 5552; 
	
	static UdpReceiveLoop instance;
	boolean isOn = false;
	
	//clientContextsByClientUuid (passed to constructor)
	//clientContextsByIP (used for dispatching packets received from expected sources)
	
	HashMap<SocketAddress, ClientContext> clientContextsByIp;
	HashMap<String, ClientContext> clientContextsByClientUuid; 
	
	//DatagramMsgChannel channel;
	
	public UdpReceiveLoop()
	{
	}
	
	public static UdpReceiveLoop getInstance()
	{
		if(instance == null)
			instance = new UdpReceiveLoop();
		return instance; 
	}

	public void start()
	{
		if(isOn)
			return;
		
		Thread t = new Thread(this);
		t.start();		
	}
	
	public void run()
	{	
		DatagramMsgChannel channel = DatagramMsgChannel.getInstance();
		isOn = true;
		while(isOn)
		{	
			Pair<SocketAddress, SerialisableMessage> pair = channel.receive();			
			
			if(pair == null || pair.second == null)
				continue;
			if(pair.second == null) //i.e invalid message. TODO: send error message back
				continue; 
			
			if(pair.second instanceof RegisterConnectionW)
			{
				RegisterConnectionW msg = (RegisterConnectionW)pair.second;
				
				if(!UserManager.getInstance().isValidUser(msg.userUuid, false)){
					System.out.println("invalid user"); //or could also be because this userUuid hasn't been cached yet (but in general this should be unlikely because the tcp connection should initiated first)
					continue;
				}
				if(!clientContextsByClientUuid.containsKey(msg.clientContextUuid)){
					System.out.println("unexpected clientContextUuid");
					continue;
				}
				clientContextsByIp.put(pair.first, clientContextsByClientUuid.get(msg.clientContextUuid));
				continue;
			}
			
			ClientContext clientContext = clientContextsByIp.get(pair.first);
			if(clientContext == null)
				continue;
			
			if(pair.second instanceof RtAudioChunkW){
				((RtAudioChunkW)pair.second).timestamp();
				clientContext.receiveChunk((RtAudioChunkW)pair.second);
			}
			else
				System.out.println("unexpected message type received on udp channel");		
		}
	}
}
