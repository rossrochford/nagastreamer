package workerloops;

import java.io.IOException;

import naga.eventmachine.EventMachine;

public class AsyncExecutor 
{
	static AsyncExecutor instance;
	
	public EventMachine eventMachine;	
	public boolean isRunning = false;
	
	public AsyncExecutor()
	{
		try {
			eventMachine = new EventMachine();
		} catch (IOException e) {e.printStackTrace();}
	}
	
	public static AsyncExecutor getInstance()
	{
		if(instance == null)
			instance = new AsyncExecutor();
		return instance;
	}
	
	public void start()
	{
		eventMachine.start();
		isRunning = true;
	}
	
	public void submit(Runnable r)
	{		
		if(!isRunning)
			System.out.println("warning: AsyncExecutor not started");
		
		eventMachine.asyncExecute(r);
	}
	
	public void submit(Runnable r, long msDelay)
	{
		eventMachine.executeLater(r, msDelay);
	}	
	
	//we could also have an executeAt() method 
}
