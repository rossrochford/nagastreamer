package workerloops;

import java.util.LinkedList;

import messages.RtAudioChunkW;
import util.Util;
import audio.LongAudioBuffer;
import audio.AudioLoop;

public abstract class ClockedRunnable implements Runnable
{
	boolean isRunning = false;
	double delayNano;
	public int iterLimit;
	
	public ClockedRunnable(double delayMs)
	{
		this(delayMs, -1);
	}
	
	public ClockedRunnable(double delayMs, int iterLimit)
	{
		this.delayNano = Util.msToNano(Math.round(delayMs)); //delayMs*1000000.0;
		this.iterLimit = iterLimit;
	}
	
	public void run() 
	{
		if(isRunning)
			return;
		isRunning = true;
		
		int iterCount = 0;
		long startTime = System.nanoTime();
		while(isRunning)
		{
			doStuff();
			
			double oughtToBe = startTime + (iterCount*delayNano);
			iterCount++;				
			double diff = System.nanoTime()-oughtToBe;		
			
			Util.sleep(Math.max(0, Math.round(((delayNano-diff)/1000000.0))));
			
			//System.out.println("ought: "+oughtToBe);System.out.println("diff: "+diff/1000000.0);
			
			if(iterLimit != -1 && iterCount >= iterLimit)
				break;
		}
		isRunning = false;
	}
	
	public abstract void doStuff();
	
	
	/*Thread t = new Thread(new Runnable() {
			AudioBuffer buf = rtPlayerTest.rtPlayer.buffer;
			LinkedList<RtAudioChunkW> chunks = rtPlayerTest.chunks;
			int chunkSize = rtPlayerTest.rtPlayer.ss.chunkSize;
			
			public void run() {
				for(int i=0; i<chunks.size(); i++){
					chunks.get(i).timestamp();
					buf.addChunk(chunks.get(i));
					Util.sleep(Math.round(Util.sampsToMs(chunkSize)));
				}				
			}
		}); t.start();*/
}
